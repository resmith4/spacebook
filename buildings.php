<?php session_start();
include 'cookout.php';
include_once 'functions.php';
include_once 'objects.php';

if($_SESSION['auth'] != 'yes'){
	include "spash_screen.php";die();
}

updatePlanetResources($_SESSION['activePlanet']);
updatePlanetConstruction($_SESSION['activePlanet']);

if($_GET['action'] == 'constructBuilding'){
	$message = startConstruction($_GET['buildingID']);
}

?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Spacebook | Buildings</title>
	<?php 	include 'linksAndScripts.php';
			include 'templates/floatingStars_JS.php';
	?>
</head>
<body>
	
	<div id="page-wrap">
		<div class='container-fluid'>
			<div class='row-fluid'>
				<?php include "templates/spacebookHeader.php" ?>
			</div>
			<div style='margin-top:15px' class='row-fluid'>
				<div class='span3'>
					<div class='planet-info-pane'>
						<?php include "templates/infoSidebar.php" ?>
					</div>
				</div>
				<div class='span9'>
					<?php echo $message; ?>
					<?php include 'templates/buildingInfoPane.php'; ?>
			</div>
		</div>
	</div>
</body>
</html>