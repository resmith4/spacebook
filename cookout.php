<?php
// Fill in all the info we need to connect to the database.
// This is the same info you need even if you're using the old mysql_ library.
$host = 'localhost';
$port = 3306; // This is the default port for MySQL
$database = 'Spacebook';
$db_username = 'root';
$db_password = 'admin';

// Construct the DSN, or "Data Source Name".  Really, it's just a fancy name
// for a string that says what type of server we're connecting to, and how
// to connect to it.  As long as the above is filled out, this line is all
// you need :)
$dsn = "mysql:host=$host;port=$port;dbname=$database";

// Connect!
// ...or try to.  Right now it will display a blank page with a
// "SYSTEM ERROR" line if something went wrong.
// In the future, we should have a pretty page saying that something
// went wrong, and log it somewhere for the user to not see the
// actual error message.
try {
	$db = new PDO($dsn, $db_username, $db_password);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	die('SYSTEM ERROR: ' . $e->getMessage());
}
?>
