<?php session_start();
include 'cookout.php';
include_once 'functions.php';
include_once 'objects.php';

if($_SESSION['auth'] != 'yes'){
	include "spash_screen.php";die();
}

updatePlanetResources($_SESSION['activePlanet']);

if (isset($_POST['action']) && $_POST['action'] == 'dispatchFleet') {
	$message = dispatchFleet();
}

if (isset($_GET['action']) && $_GET['action'] == "cancelFleet") {
	$message = cancelFleet($_GET['fleetID']);
}

?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Spacebook</title>
	<?php 	include 'linksAndScripts.php';
			include 'templates/floatingStars_JS.php';
	?>
</head>
<body>
	<?php
	include 'templates/floatingStars_HTML.php';
	$resources = echoAvailableResources($_SESSION['activePlanet']);
	echoPlanetLocation($_SESSION['activePlanet']);
	?>

	<div id="page-wrap">
		<div class='container-fluid'>
			<div class='row-fluid'>
				<?php include "templates/spacebookHeader.php" ?>
			</div>
			<div style='margin-top:15px' class='row-fluid'>
				<div class='span3'>
					<?php include "templates/infoSidebar.php" ?>
				</div>
				<div class='span9'>
					<?php  ?>
					<?php include 'templates/fleetInfo.php' ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
