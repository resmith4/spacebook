<?php
include 'cookout.php';
include_once 'objects.php';
$shipClasses = array(
"Apis",
"Culicidae",
"Vespula",
"Vespa",
"Hymenoptera",
"Lupus" ,
"Jamaicensis" ,
"Serpentes" ,
"Mississippiensis" ,
"Tigris" ,
"Leo" ,
"Gorilla" ,
"Capensis" ,
"Capra" ,
"Asinus" ,
"Caballus" ,
"Capra-P",
"Primigenius" ,
"Caballus-P",
"Elephantidae" ,
"Tarandus",
"Tarandus-P",
"Macrorhynchos" ,
"Livia" ,
"Africanus" ,
"Strigiformes" ,
"Latrans" ,
"Maritimus" ,
"Macrorhynchos-S"
);

$ships_fighters = array(
	"Apis",
	"Culicidae",
	"Vespula",
	"Vespa",
	"Hymenoptera"
	);

$ships_lightCruisers = array(
	"Lupus" ,
	"Jamaicensis" ,
	"Serpentes" ,
	"Mississippiensis"
);

$ships_heavyCruisers = array(
	"Tigris" ,
	"Leo" ,
	"Gorilla"
);

$ships_dreadnaughts = array(
	"Capensis"
);

$ships_transport = array(
	"Capra" ,
	"Asinus" ,
	"Caballus" ,
	"Capra-P",
	"Primigenius" ,
	"Caballus-P",
	"Elephantidae" ,
	"Tarandus"
);

$ships_special = array(
	"Macrorhynchos" ,
	"Livia" ,
	"Africanus" ,
	"Strigiformes" ,
	"Latrans" ,
	"Maritimus" ,
	"Macrorhynchos-S"
);

function get_planet_fleet_ships($planetID){
	include "cookout.php";
	$getPlanetInfo = $db->prepare("SELECT * FROM `planets` WHERE `ID` = ?");
	$getPlanetInfo->execute(array($planetID));
	$row = $getPlanetInfo->fetch(PDO::FETCH_ASSOC);
	return $row;
}

function getPlanetFleetShips($planetID){
	include 'cookout.php';
	$final = array();
	//get ship categories
	$getCategories = $db->prepare("SELECT `ID`,`name`,`letterCode` FROM `shipTypes` WHERE `canDispatch` = ?");
	$getCategories->execute(array("yes")) or die(var_dump($db->errorInfo()));
	while($categoryRow = $getCategories->fetch(PDO::FETCH_ASSOC)){
		//once in each category $categoryRow['ID'], get each type of ship in the category
		$categoryPush = array();
		$categoryPush['ships'] = array();
		$categoryPush['name'] = $categoryRow['name'];
		$categoryPush['name'] = ucfirst($categoryPush['name']); //capitalizes the name of the shipType
		$categoryPush['name'] = $categoryPush['name'].'s'; //makes the name of the shiptype plural
		$categoryPush['letterCode'] = $categoryRow['letterCode'];
		$getShips = $db->prepare("SELECT * FROM `shipClasses` WHERE `shipType` = ?");
		$getShips->execute(array($categoryRow['ID'])) or die(var_dump($db->errorInfo()));
		while($shipClassRow = $getShips->fetch(PDO::FETCH_ASSOC)){
			//now that we have a ship class for each, lets search and see if they have any parked ships
			$getShipNumber = $db->prepare("SELECT * FROM `parkedShips` WHERE `shipID` = ? AND `planetID` = ?");
			$getShipNumber->execute(array($shipClassRow['ID'],$planetID));

			$num = $getShipNumber->rowCount();
			if($num > 0){
				$shipNumberRow = $getShipNumber->fetch(PDO::FETCH_ASSOC);
				//row was returned, get the value from that 
				$shipPush = array();
				$shipPush['name'] = $shipClassRow['name'];
				$shipPush['shipClassID'] = $shipClassRow['ID'];
				$shipPush['numberOfShips'] = $shipNumberRow['number'];
				array_push($categoryPush['ships'],$shipPush);		
			}
		}
		$count = count($categoryPush['ships']);
		if($count > 0){
			array_push($final,$categoryPush);
		}
	}
	return $final;
}



function set_active_planet($planetID){
	include 'cookout.php';
	$checkOwnership = $db->prepare("SELECT * FROM `planets` WHERE `colonizedBy` = ?");
	$checkOwnership->execute(array($_SESSION['UID']));
	$num = $checkOwnership->rowCount();
	if($num > 0){
		//planet is owned by the logged in user
		$_SESSION['activePlanet'] = $planetID;
	}else{
		echo "Error setting active planet. Planet not owned by active user";die();
	}
}

function set_first_user_planet(){
	include 'cookout.php';
	$getFirst = $db->prepare("SELECT `ID` FROM `planets` WHERE `colonizedBy` = ? LIMIT 1");
	$getFirst->execute(array($_SESSION['UID']));
	$row = $getFirst->fetch(PDO::FETCH_ASSOC);
	set_active_planet($row['ID']);
}

function login_user($username,$password){
	include 'cookout.php';
	$getUser = $db->prepare("SELECT * FROM `users` WHERE `username` = ?");
	$getUser->execute(array($username))or die(var_dump($getUser->errorInfo()));
	$num = $getUser->rowCount();
	if($num > 0){
		//username found
		$row = $getUser->fetch(PDO::FETCH_ASSOC);
		if($row['password'] == md5($password)){
			//password matches
			##Setting The Info Variables ##
			$_SESSION['auth'] = 'yes';
			$_SESSION['UID'] = $row['ID'];
			$_SESSION['username'] = $row['username'];
			##Setting the active planet to the first colinized planet##
			set_first_user_planet();

		}else{
			return "Username or Password not found";
		}

	}else{
		//username not found
		return "Username or Password not found";
	}
}

function getCredits($userID){
	include 'cookout.php';
	$getCredits = $db->prepare("SELECT `credits` FROM `users` WHERE `ID` = ?");
	$getCredits->execute(array($userID)) or die(var_dump($getCredits->errorInfo()));
	$row = $getCredits->fetch(PDO::FETCH_ASSOC);
	return $row['credits'];
}

function getTributeSpins($userID){
	include 'cookout.php';
	$getTributeSpins = $db->prepare("SELECT `tributeSpins` FROM `users` WHERE `ID` = ?");
	$getTributeSpins->execute(array($userID)) or die(var_dump($getTributeSpins->errorInfo()));
	$row = $getTributeSpins->fetch(PDO::FETCH_ASSOC);
	return $row['tributeSpins'];
}

function getUserPlanets($userID){
	include 'cookout.php';
	$getPlanets = $db->prepare("SELECT * FROM `planets` WHERE `colonizedBy` = ?");
	$getPlanets->execute(array($userID)) or die(var_dump($db->errorInfo()));
	$final = array();
	while($row = $getPlanets->fetch(PDO::FETCH_ASSOC)){
		array_push($final,$row);
	}

	return $final;
}

function getPlanetResourceArray($planetID){
	include 'cookout.php';
	$getPlanetInfo = $db->prepare("SELECT * FROM `planets` WHERE `ID` = ?");
	$getPlanetInfo->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getPlanetInfo->fetch(PDO::FETCH_ASSOC);
	$final = array(
		'ore'=> $row['available_Ore'],
		'crystal' => $row['available_Crystal'],
		'hydrogen' => $row['available_Hydrogen'],
		'antimatter' => $row['available_Antimatter'],
		'energyAvailable' => $row['available_Energy']);
	return $final;
};

function getEmpireName($userID){
	include 'cookout.php';
	$getEmpireName = $db->prepare("SELECT * FROM `users` WHERE `ID` = ?");
	$getEmpireName->execute(array($userID));
	$row = $getEmpireName->fetch(PDO::FETCH_ASSOC);
	return $row['empireName'];
}

function getPlanetName($planetID){
	include 'cookout.php';
	$getEmpireName = $db->prepare("SELECT * FROM `planets` WHERE `ID` = ?");
	$getEmpireName->execute(array($planetID));
	$row = $getEmpireName->fetch(PDO::FETCH_ASSOC);
	return $row['name'];

}

function getPlanetFields($planetID){
	include 'cookout.php';
	$getFields = $db->prepare("SELECT `total_fields`,`available_fields` FROM `planets` WHERE `ID` = ?");
	$getFields->execute(array($planetID));
	$row = $getFields->fetch(PDO::FETCH_ASSOC);
	$usedFields = $row['total_fields'] - $row['available_fields'];
	//$usedFields = "$usedFields";
	return "$usedFields/".$row['total_fields'];
}

function getFleetInfo($userID){
	include 'cookout.php';
	$getFleets = $db->prepare("SELECT `total_fleets`,`available_fleets` FROM `users` WHERE `ID` = ?");
	$getFleets->execute(array($userID));
	$row = $getFleets->fetch(PDO::FETCH_ASSOC);
	$usedFleets = $row['total_fleets'] - $row['available_fleets'];
	return "$usedFleets/".$row['total_fleets'];
}

function getProductionInfo($planetID){
	include 'cookout.php';
	$getProduction = $db->prepare("SELECT * FROM `planets` WHERE `ID` = ?");
	$getProduction->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getProduction->fetch(PDO::FETCH_ASSOC);
	$energy = $row['available_Energy'] - $row['energy_use'];
	$energy = "$energy";
	$final = array(
		"ore"=>$row['ore_production'],
		"crystal"=>$row['crystal_production'],
		"hydrogen"=>$row['hydrogen_production'],
		"antimatter"=>$row['antimatter_production'],
		"energyUse"=>$energy);
	return $final;
}

function echoAvailableResources($planetID){
	include 'cookout.php';
	$getResourceInfo = $db->prepare("SELECT `available_Ore`,`available_Crystal`,`available_Hydrogen` FROM `planets` WHERE `ID` = ?");
	$getResourceInfo->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	while($row = $getResourceInfo->fetch(PDO::FETCH_ASSOC)){
		echo "\n";
		foreach($row as $key=>$value){
			echo "<input type='hidden' id='$key' value='$value'>\n";
		}
	}
	return $row;	
}

function echoPlanetLocation($planetID){
	include 'cookout.php';
	$getLocation = $db->prepare("SELECT `location_galaxy`,`location_system`,`location_planet` FROM `planets` WHERE `ID` = ?");
	$getLocation->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getLocation->fetch(PDO::FETCH_ASSOC);
	echo "<input type='hidden' name='active_planet_location' id='active_planet_location' value='".$row['location_galaxy'].":".$row['location_planet'].":".$row['location_planet']."'>";
}

function getFleetSpeed($ships,$distance){
	$shipSpeeds = array();
	foreach($ships as $value){
		if($value['numberOfShips'] > 0){
			include 'cookout.php';
			$getShipSpeed = $db->prepare("SELECT * FROM `shipClasses` WHERE `ID` = ?");
			$getShipSpeed->execute(array($value['shipClassID'])) or die(var_dump($db->errorInfo()));
			$row = $getShipSpeed->fetch(PDO::FETCH_ASSOC);
			array_push($shipSpeeds,$row['base_speed']);
		}
	}
	sort($shipSpeeds);
	$travelTime = ($distance * 100) / $shipSpeeds[0];
	return($travelTime);
}

function getFleetFuelConsumption($ships,$distance){
	$fuelConsumption = 0;
	foreach($ships as $value){
		include 'cookout.php';
		$getShipSpeed = $db->prepare("SELECT * FROM `shipClasses` WHERE `ID` = ?");
		$getShipSpeed->execute(array($value['shipClassID'])) or die(var_dump($db->errorInfo()));
		$row = $getShipSpeed->fetch(PDO::FETCH_ASSOC);
		$fuelConsumption += ($row['base_fuel_consumption'] * $value['numberOfShips'] * $distance)/100;
		$fuelConsumption = floor($fuelConsumption);
	}
	return($fuelConsumption);
}

function getFleetCargoSpace($ships){
	$cargoSpace = 0;
	foreach($ships as $value){
		include 'cookout.php';
		$getShipCargoCapacity = $db->prepare("SELECT `base_cargo_capacity` FROM `shipClasses` WHERE `ID` = ? limit 1");
		$getShipCargoCapacity->execute(array($value['shipClassID'])) or die(var_dump($db->errorInfo()));
		$row = $getShipCargoCapacity->fetch(PDO::FETCH_ASSOC);
		if($row['base_cargo_capacity'] > 20){
			$cargoSpace += ($row['base_cargo_capacity'] * $value['numberOfShips']);
		}
	}
	return($cargoSpace);
}

function calculateDistance($start,$end){
	$origin = explode(":",$start);
	$target = explode(":",$end);
	//$array[0] = galaxy, $array[1] = planet, $array[2] = system

	if($origin[0] == $target[0]){
		//target is in the same galaxy
		if($origin[1] == $target[1]){
			//target is in the same system
			if($origin[2] == $target[2]){
				//target is the same planet as the origin. Weird. but whatever.
				$distance = 1;
			}else{
				//target is different planet in same system
				$distance = abs($origin[2] - $target[2]) * 10;
			}
		}else{
			//target is a different system in the same galaxy
			$distance = abs($origin[1] - $target[2]) * 1000;
		}
	}else{
		//target is a different galaxy
		$distance = abs($origin[0] - $target[1]) * 1000000;
	}
	return $distance;
}

function getPlanetLocationString($planetID){
	include 'cookout.php';
	$getLocation = $db->prepare("SELECT `location_galaxy`,`location_system`,`location_planet` FROM `planets` WHERE `ID` = ?");
	$getLocation->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getLocation->fetch(PDO::FETCH_ASSOC);
	$outputString = $row['location_galaxy'].":".$row['location_system'].":".$row['location_planet'];
	return $outputString;
}

function getPlanetLocationArray($planetID){
	include 'cookout.php';
	$getLocation = $db->prepare("SELECT `location_galaxy`,`location_system`,`location_planet` FROM `planets` WHERE `ID` = ?");
	$getLocation->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getLocation->fetch(PDO::FETCH_ASSOC);
	$output = array(
		'galaxy'=>$row['location_galaxy'],
		'system'=>$row['location_system'],
		'planet'=>$row['location_planet']);
	return $output;
}

function buildShipsArrayFromPost(){
	$ships = array();
	foreach($_POST as $key => $value){
		if(substr($key,0,6) == "ships_"){
			if($value > 0){
				$num = substr($key,6);
				$info = array();
				$info['shipClassID'] = $num;
				$info['numberOfShips'] = $value;
				array_push($ships,$info);
			}
		}
	}
	return $ships;
}

function fuelCheck($fuelUse){
	include 'cookout.php';
	$getFuel = $db->prepare("SELECT `available_Hydrogen` FROM `planets` WHERE `ID` = ".$_SESSION['activePlanet']);
	$getFuel->execute(array());
	$row = $getFuel->fetch(PDO::FETCH_ASSOC) or die(var_dump($db->errorInfo()));
	if($row['available_Hydrogen'] < $fuelUse){
		//not enough fuel on planet
		return "no";
	}else{
		//enough fuel on planet
		return "yes";
	}
}

function shipNumberCheck($ships){
	include 'cookout.php';
	foreach($ships as $value){
		$getNumberOfShipsParked = $db->prepare("SELECT `number` FROM `parkedShips` WHERE `shipID` = ? AND `planetID` = ?");
		$getNumberOfShipsParked->execute(array($value['shipClassID'],$_SESSION['activePlanet'])) or die(var_dump($db->errorInfo()));
		$row = $getNumberOfShipsParked->fetch(PDO::FETCH_ASSOC);
		if($row['number'] < $value['numberOfShips']){
			return "no";
		}
	}
	return "yes";
}

function cargoSpacecheck($ships,$resourcesLoaded){
	$cargoSpaceAvailable = getFleetCargoSpace($ships);
	if($resourcesLoaded['total'] == 0){
		return "yes";
	}
	if($cargoSpaceAvailable > $resourcesLoaded['total']){
		return "yes";
	}else{
		return "no";
	}
}

function dispatchFleet(){
	include 'cookout.php';
	//exiting the function if no orders were selected
	if($_POST['fleetOrders'] == "None"){
		$message = array("type"=>"danger","message"=>"No orders were given. Fleet not dispatched.");
		return $message;
	}
	$ships = buildShipsArrayFromPost();
	extract($_POST);
	//building Info
	$targetString = $_POST["destination-galaxy"].":".$_POST["destination-system"].":".$_POST["destination-planet"];
	$originString = getPlanetLocationString($_SESSION['activePlanet']);
	$distance = calculateDistance($originString,$targetString);
	$fuelUse = getFleetFuelConsumption($ships,$distance);
	$fleetTravelTime = getFleetSpeed($ships,$distance);
	$targetPlanetID = getPlanetIDFromArray(array($_POST["destination-galaxy"],$_POST["destination-system"],$_POST["destination-planet"]));
	//check to make sure there is enough fuel available on the planet to launch the fleet
	$enoughFuel = fuelCheck($fuelUse);
	if($enoughFuel == "no"){
		$message = array("type"=>"danger","message"=>"There is not enough hydrogen on your planet to launch your fleet.");
		return $message;
	}
	//check to make sure that the necessary ships are parked at the planet
	$enoughShips = shipNumberCheck($ships);
	if($enoughShips == "no"){
		$message = array("type"=>"danger","message"=>"You do not have enough ships parked at your planet to launch your fleet");
		return $message;
	}

	$resourcesLoaded = array(
		"ore"=>$_POST["ore-sent-input"],
		"crystal"=>$_POST["crystal-sent-input"],
		"hydrogen"=>$_POST["hydrogen-sent-input"],
		"total"=>$_POST["ore-sent-input"] + $_POST["crystal-sent-input"] + $_POST["hydrogen-sent-input"]
		);
	$enoughCargoSpace = cargoSpaceCheck($ships,$resourcesLoaded);
	if($enoughCargoSpace == "no"){
		$message = array("type"=>"danger","message"=>"There is not enough cargo space available to transport that amount of resources. Fleet not dispatched.");
		return $message;
	}
	//create fleet
	$createFleet = $db->prepare("INSERT INTO `fleets` (`userID`,`originPlanetID`,`targetPlanetID`,`type`,`dispatch`,`arrival`,`ore_loaded`,`crystal_loaded`,`hydrogen_loaded`) VALUES (?,?,?,?,NOW(),NOW() + INTERVAL $fleetTravelTime MINUTE ,".$resourcesLoaded['ore'].",".$resourcesLoaded['crystal'].",".$resourcesLoaded['hydrogen'].") ");
	$createFleet->execute(array($_SESSION['UID'],$_SESSION['activePlanet'],$targetPlanetID,$_POST['fleetOrders'])) or die(var_dump($db->errorInfo()));


	switch($_POST['fleetOrders']){
		case "Transport":
		case "Attack":
		$fleetID = $db->lastInsertId();
		//create the return trip as well
		$returnTripTravelTime = $fleetTravelTime * 2;
		$createReturnTrip = $db->prepare("INSERT INTO `fleets` (`userID`,`originPlanetID`,`targetPlanetID`,`type`,`dispatch`,`arrival`,`returnTripOf`) VALUES (?,?,?,?,NOW()+ INTERVAL $fleetTravelTime MINUTE ,NOW() + INTERVAL $returnTripTravelTime MINUTE ,?) ");
		$createReturnTrip->execute(array($_SESSION['UID'],$targetPlanetID,$_SESSION['activePlanet'],'Return',$fleetID)) or die(var_dump($db->errorInfo()));

	}
	$fleetID = $db->lastInsertId();

	foreach($ships as $value){		//remove the ships from the planet
		$removeShipsFromPlanet = $db->prepare("UPDATE `parkedShips` SET number = number - ".$value['numberOfShips']." WHERE `planetID` = ? AND `shipID` = ?");
		$removeShipsFromPlanet->execute(array($_SESSION['activePlanet'],$value['shipClassID'])) or die(var_dump($db->errorInfo()));
		//add the ships to the fleet 
		$insertShipFleetRow = $db->prepare("INSERT INTO `movingShips` (`shipID`,`fleetID`,`number`) VALUES (?,?,?) ");
		$insertShipFleetRow->execute(array($value['shipClassID'],$fleetID,$value['numberOfShips'])) or die(var_dump($db->errorInfo()));
		}
	$message = array("type"=>"success","message"=>"Fleet Dispatched.");
	return $message;
}

function updatePlanetResources($planetID){
	include 'cookout.php';
	$getTimeAndResources = $db->prepare("SELECT `last_update`,`ore_production`,`crystal_production`,`hydrogen_production` FROM `planets` WHERE `ID` = ?");
	$getTimeAndResources->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getTimeAndResources->fetch(PDO::FETCH_ASSOC);
	$previousLook = $row['last_update'];
	$currentLook = time();
	//$currentLook = strtotime($currentLook);
	$previousLook = strtotime($previousLook);

	//the time between the two looks, in minutes.
	$timeBetweenLooks = ($currentLook - $previousLook) / 60;

	$oreProduced = ($row['ore_production'] / 60) * $timeBetweenLooks;
	$crystalProduced = ($row['crystal_production'] / 60) * $timeBetweenLooks;
	$hydrogenProduced = ($row['hydrogen_production'] / 60) * $timeBetweenLooks;

	$updateResources = $db->prepare("UPDATE `planets` SET `available_Ore` = `available_Ore` + ?, `available_Crystal` = `available_Crystal` + ?, `available_Hydrogen` = `available_Hydrogen` + ?, `last_update` = NOW() WHERE `ID` = ?");
	$updateResources->execute(array($oreProduced,$crystalProduced,$hydrogenProduced,$planetID)) or die(var_dump($db->errorInfo()));

}

function getPlanetIDFromArray($array){
	include 'cookout.php';
	$getID = $db->prepare("SELECT `ID` FROM `planets` WHERE `location_galaxy` = ? AND `location_system` = ? AND `location_planet` = ?");
	$getID->execute($array);
	$row = $getID->fetch(PDO::FETCH_ASSOC);
	return $row['ID'];
}

function secondsLeftIntoTime($seconds){
	if($seconds < 60){
		//time is less than a minute
		$seconds = "0".$seconds;
		$seconds = substr($seconds,-2);
		return "00:00:".$seconds;
	} else {
		if($seconds < (60*60)){
			//time is less than an hour
			$minutes = floor($seconds/60);
			$seconds = floor($seconds%60);
			$minutes = "0".$minutes;
			$seconds = "0".$seconds;
			$minutes = substr($minutes,-2);
			$seconds = substr($seconds,-2);
			return "00:".$minutes.":".$seconds;
		} else {
			//time is more than an hour
			$hours = floor($seconds/(60*60));
			$seconds = $seconds - ($hours * (60 * 60));
			$minutes = floor($seconds/60);
			$seconds = floor($seconds%60);

			$hours = "0".$hours;
			$minutes = "0".$minutes;
			$seconds = "0".$seconds;
			$hours = substr($hours,-2);
			$minutes = substr($minutes, -2);
			$seconds = substr($seconds, -2);
			return $hours.":".$minutes.":".$seconds;
		}
	}
}

function TimeIntoSecondsLeft($timeLeft){
	$timeArray = explode(":",$timeLeft);
	$seconds = ($timeArray[0] * 60 * 60) + ($timeArray[1] * 60) + $timeArray[2];
	return $seconds;
}

function generateShipsFromFleetRow($row){
	$output = array();
	foreach($row as $key=>$value){
		if(substr($key,0,6) == "ships_"){
			if($value > 0){
				$input = array($key,$value);
				array_push($output,$input);
			}
		}
	}
	return $output;
}

function addShipsToPlanet($planetID,$shipID,$number){
	include 'cookout.php';
	$searchForShip = $db->prepare("SELECT * FROM `parkedShips` WHERE `planetID` = ? AND `shipID` = ?");
	$searchForShip->execute(array($planetID,$shipID)) or fail();
	$num = $searchForShip->rowCount();
	$row = $searchForShip->fetch(PDO::FETCH_ASSOC);
	if($num > 0){
		//row exists, update
		$updateRow = $db->prepare("UPDATE `parkedShips` SET `number` = `number` + $number WHERE `ID` = ?");
		$updateRow->execute(array($row['ID'])) or fail();
	}else{
		//row does not exist, insert
		$insertRow = $db->prepare("INSERT INTO `parkedShips` (`shipID`,`planetID,`,`number`) VALUES (?,?,?)");
		$insertRow->execute(array($shipID,$planetID,$number));
	}
}

function processFleetArrival($fleetID){
	$putShipsOnPlanet = function($planetID,$shipClassID,$number){
		include 'cookout.php';
		$checkForShipRow = $db->prepare("SELECT * FROM `parkedShips` WHERE `planetID` = ? AND `shipID` = ?");
		$checkForShipRow->execute(array($planetID,$shipClassID)) or die(var_dump($db->errorInfo()));
		$num = $checkForShipRow->rowCount();
		if($num > 0){
			//row exists, so update the existing row
			$updateShipRow = $db->prepare("UPDATE `parkedShips` SET `number` = `number` + $number WHERE `planetID` = ? AND `shipID` = ?");
			$updateShipRow->execute(array($planetID,$shipClassID)) or die(var_dump($db->errorInfo()));
		}else{
			//no row exists, create the row
			$insertShipRow = $db->prepare("INSERT INTO `parkedShips` (`planetID`,`shipID`,`number`) VALUES (?,?,?)");
			$insertShipRow->execute(array($planetID,$shipClassID,$number)) or die(var_dump($db->errorInfo()));

		}
	};

	$pullShipsFromFleet = function($fleetID,$shipClassID,$number,$toFleetID = null){
		//the toFleetID param is for ships that are being moved from one fleet to another. 
		include 'cookout.php';
		if(isset($toFleetID)){
			$checkForShipRow = $db->prepare("SELECT * FROM `movingShips` WHERE `fleetID` = ? AND `shipID` = ?");
			$checkForShipRow->execute(array($toFleetID,$shipClassID)) or die(var_dump($db->errorInfo()));
			$num = $checkForShipRow->rowCount();
			if($num > 0){
				//row is found, there are already ships of this type in a fleet, update the row
				$updateShipRow = $db->prepare("UPDATE `movingShips` SET `number` = `number` + $number WHERE `fleetID` = ? AND `shipID` = ?");
				$updateShipRow->execute(array($toFleetID,$shipClassID)) or die(var_dump($db->errorInfo()));
			}else{
				//row is not found, create the row
				$insertShipRow = $db->prepare("INSERT INTO `movingShips` (`shipID`,`fleetID`,`number`) VALUES (?,?,?) ");
				$insertShipRow->execute(array($shipClassID,$toFleetID,$number));
			}
		}
		$deleteShipRow = $db->prepare("DELETE FROM `movingShips` WHERE `shipID` = ? AND `fleetID` = ?");
		$deleteShipRow->execute(array($shipClassID,$fleetID)) or die(var_dump($db->errorInfo()));
	};

	$dumpResourcesOnPlanet = function($fleetID,$planetID){
		include 'cookout.php';
		$getFleetResources = $db->prepare("SELECT `ore_loaded`,`crystal_loaded`,`hydrogen_loaded` FROM `fleets` WHERE `ID` = ?");
		$getFleetResources->execute(array($fleetID)) or die(var_dump($db->errorInf()));
		$fleetResourceRow = $getFleetResources->fetch(PDO::FETCH_ASSOC);
		$updateString = "";
		if(isset($fleetResourceRow['ore_loaded']) and ($fleetResourceRow['ore_loaded'] > 0)){
			$updateString = $updateString."`available_Ore` = `available_Ore` + ".$fleetResourceRow['ore_loaded']." ";
		}
		if(isset($fleetResourceRow['crystal_loaded']) and ($fleetResourceRow['crystal_loaded']) > 0){
			$updateString = $updateString."`available_Crystal` = `available_Crystal` + ".$fleetResourceRow['crystal_loaded']." ";
		}
		if(isset($fleetResourceRow['hyrdogen_loaded']) and ($fleetResourceRow['hydrogen_loaded']) > 0){
			$updateString = $updateString."`available_Hyrdogen` = `available_Hyrdogen` + ".$fleetResourceRow['hyrdogen_loaded']." ";
		}

		if(strlen($updateString) > 0){
			$placeResourcesOnPlanet = $db->prepare("UPDATE `planets` SET $updateString WHERE `ID` = ? ");
			$placeResourcesOnPlanet->execute(array($planetID)) or die(var_dump($db->errorInfo()));
		}
	};

	include 'cookout.php';
	$getFleetInfo = $db->prepare("SELECT * FROM `fleets` WHERE `ID` = ?");
	$getFleetInfo->execute(array($fleetID));
	$fleetInfoRow = $getFleetInfo->fetch(PDO::FETCH_ASSOC);

	switch($fleetInfoRow['type']){
		case "Return":
		case "Deploy":
		//ships are staying at the location.
			//move the ships
			$getShipsInFleet = $db->prepare("SELECT * FROM `movingShips` WHERE `fleetID` = ?");
			$getShipsInFleet->execute(array($fleetInfoRow['ID'])) or die(var_dump($db->errorInfo()));
			while($shipsInFleetInfoRow = $getShipsInFleet->fetch(PDO::FETCH_ASSOC)){
				$putShipsOnPlanet($fleetInfoRow['targetPlanetID'],$shipsInFleetInfoRow['shipID'],$shipsInFleetInfoRow['number']);
				$pullShipsFromFleet($fleetInfoRow['ID'],$shipsInFleetInfoRow['shipID'],$shipsInFleetInfoRow['number']);
			}
			//dump the resources
			$dumpResourcesOnPlanet($fleetInfoRow['ID'],$fleetInfoRow['targetPlanetID']);
			break;

		case "Transport":

		//ships are dropping off supplies.
			//put the resources on the planet
			$dumpResourcesOnPlanet($fleetInfoRow['ID'],$fleetInfoRow['targetPlanetID']);

			//get return trip ID
			$getReturnTripID = $db->prepare("SELECT `ID` FROM `fleets` WHERE `returnTripOf` = ?");
			$getReturnTripID->execute(array($fleetInfoRow['ID'])) or die(var_dump($db->errorInfo()));
			$returnTripIDRow = $getReturnTripID->fetch(PDO::FETCH_ASSOC);

			//switch the ships in the fleet to the return trip
			$switchShips = $db->prepare("UPDATE `movingShips` SET `fleetID` = ? WHERE `fleetID` = ?");
			$switchShips->execute(array($fleetInfoRow['ID'],$returnTripIDRow['ID'])) or die(var_dump($db->errorInfo()));
			break;

		case "Harvest":
			break;
	}
	//delete the fleet row
	$deleteString = "DELETE FROM `fleets` WHERE `ID` = ?";
	$removeFleet = $db->prepare($deleteString);
	$removeFleet->execute(array($fleetID));
}

function cancelFleet($fleetID){
	include 'cookout.php';
	$getFleetInfo = $db->prepare("SELECT * FROM `fleets` WHERE `ID` = ?");
	$getFleetInfo->execute(array($fleetID));
	$row = $getFleetInfo->fetch(PDO::FETCH_ASSOC);
	$now = time();
	$fleetTravelTime = $now - strtotime($row['dispatch']);

	$minutes_cancel = ceil($fleetTravelTime/60);

	$shipNameList_cancelFleet = "";

	foreach($row as $key=>$value){
		if(substr($key,0,6) == "ships_"){
			$shipNameList_cancelFleet = $shipNameList_cancelFleet.",`$key`";
			$shipNumberList_cancelFleet = $shipNumberList_cancelFleet.",$value";
		}
	}
	$insertString = "INSERT INTO `fleets` (`userID`,`originPlanetID`,`targetPlanetID`,`type`,`dispatch`,`arrival`,`returnTripOf`".$shipNameList_cancelFleet.") VALUES (".$row['userID'].",".$row['originPlanetID'].",".$row['targetPlanetID'].",'Return','".$row['dispatch']."',NOW() + INTERVAL $minutes_cancel MINUTE,NULL".$shipNumberList_cancelFleet.")";

	$setReturnFleet = $db->prepare($insertString);
	$setReturnFleet->execute(array());

	$deletePrimary = $db->prepare("DELETE FROM `fleets` WHERE `ID` = ?");
	$deleteReturn = $db->prepare("DELETE FROM `fleets` WHERE `returnTripOf` = ?");

	$deletePrimary->execute(array($row['ID']));
	$deleteReturn->execute(array($row['ID']));
}

function getPlanetActivities($planetID){
	include 'cookout.php';

	/*
		Format -
		'title' - "Fleet Leaving"
		'startTime' - startTime
		'endTime' - end Time
		'actionLinks' - array()
		//the action link will go at the end of the /action.php?type=fleet&action=cancel&ID=23
		//input as "cancel" => "type=fleet&action=cancel&ID=23"

	*/

	$activities = array();
	//get ships that are coming to the planet
	$getShipsComing = $db->prepare("SELECT * FROM `fleets` WHERE `targetPlanetID` = ?");
	$getShipsComing->execute(array($planetID));
	while($row = $getShipsComing->fetch(PDO::FETCH_ASSOC)){
		#getPlanetName
		$input = array();
		$targetPlanetName = getPlanetName($row['targetPlanetID']);
		$input['title'] = "Fleet on ".$row['type']." mission, headed to $targetPlanetName";
		$input['startTime'] = strtotime($row['dispatch']);
		$input['endTime'] = strtotime($row['arrival']);
		$actionLinks = array();
		$actionLinks[0]['action'] = "cancel";
		$actionLinks[0]['link'] = "type=fleet&action=cancel&ID=".$row['ID'];
		$input['actionLinks'] = $actionLinks;
		array_push($activities,$input);
	}

	//get ships that are leaving from the planet
	$getShipsLeaving = $db->prepare("SELECT * FROM `fleets` WHERE `originPlanetID` = ?");
	$getShipsLeaving->execute(array($planetID));
	while($row = $getShipsLeaving->fetch(PDO::FETCH_ASSOC)){
		#getPlanetName
		$input = array();
		$targetPlanetName = getPlanetName($row['targetPlanetID']);
		$input['title'] = "Fleet on ".$row['type']." mission, headed to $targetPlanetName";
		$input['startTime'] = strtotime($row['dispatch']);
		$input['endTime'] = strtotime($row['arrival']);
		$actionLinks = array();
		$actionLinks[0]['action'] = "cancel";
		$actionLinks[0]['link'] = "type=fleet&action=cancel&ID=".$row['ID'];
		$input['actionLinks'] = $actionLinks;
		array_push($activities,$input);
	}
	updateFleets($planetID);

	return $activities;
}

function updateFleets($planetID){
	include 'cookout.php';
	$getFleets = $db->prepare("SELECT * FROM `fleets` WHERE `originPlanetID` or `targetPlanetID` = ?");
	$getFleets->execute(array($planetID));
	while($row = $getFleets->fetch(PDO::FETCH_ASSOC)){
		$arrival = strtotime($row['arrival']);
		$now = time();
		if($arrival <= $now){
			processFleetArrival($row['ID']);
		}
	}
}

function updateUser($userID){
	include 'cookout.php';
	$getFleets = $db->prepare("SELECT * FROM `planets` WHERE `colonizedBy` = ? ");
	$getFleets->execute(array($userID));
	while($row = $getFleets->fetch(PDO::FETCH_ASSOC)){
		updateFleets($row['ID']);
	}
}

function getSystemScanArray($galaxyID,$systemID){
	$output = array();
	include 'cookout.php';
	$getSystemPlanets = $db->prepare("SELECT * FROM `planets` WHERE `location_galaxy` = ? AND `location_system` = ? ORDER BY `location_planet` ASC");
	$getSystemPlanets->execute(array($galaxyID,$systemID));
	while($row = $getSystemPlanets->fetch(PDO::FETCH_ASSOC)){
		array_push($output,$row);
	}
	return $output;
}

function getPlanetOwnerName($planetID){
	include 'cookout.php';
	$getOwnerName = $db->prepare("SELECT `username` FROM `users` WHERE `ID` IN (SELECT `colonizedBy` FROM `planets` WHERE `ID` = ?)");
	$getOwnerName->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$row = $getOwnerName->fetch(PDO::FETCH_ASSOC);
	return $row['username'];
}

function generateScanTableRows($systemArray){
	foreach($systemArray as $value){
		echo "<tr>";
		//check to see if planet is occupied
		if($value['colonizedBy'] == 0){
			//uncolonized
			echo "<td>".$value['location_planet']."</td><td colspan='5'>Uninhabited</a></td>";
		}else{
			$ownerName = getPlanetOwnerName($value['ID']);
			echo "<td>".$value['location_planet']."</td><td>".$value['name']."</td><td>$ownerName</td><td>Inhabited</td><td>-none-</td><td>-none-</td>";
		}
		echo "</tr>";
	}
}

function getEmpireResearchList($userID){
	include 'cookout.php';

	/*
	Output - Array
	[name] - the name of the tech
	[description] - the description of the tech
	[image] - the url of the image of the tech
	[ID]- the ID of the tech
	[prereqs] - array of the things needed for the tech to be researched
	*/

	$getTechs = $db->prepare("SELECT `techID`,`level` FROM `empireTechs` WHERE `userID` = ?");
	$getTechs->execute(array($userID)) or die(var_dump($db->errorInfo()));
	$final = array();
	while($row = $getTechs->fetch(PDO::FETCH_ASSOC)){
		if($row['level'] < 10){
			//tech level is below the max (currently 10)
			$getNextLevel = $db->prepare("SELECT * FROM `gameTechnology` WHERE `baseTech` = ? AND `baseTechLevel` = ?");
			$nextLevel = $row['level'] + 1;
			$getNextLevel->execute(array($row['techID'],$nextLevel)) or die(var_dump($db->errorInfo()));
			$row2 = $getNextLevel->fetch(PDO::FETCH_ASSOC);
			$output = array();
			$output['name'] = $row2['name'];
			$output['description'] = $row2['description'];
			$output['image'] = $row2['image'];
			$output['ID'] = $row2['ID'];
			$getPrereqs = $db->prepare("SELECT * FROM  `gameTechnologyRequirements` WHERE `requirementType` = 'tech' AND `requirementID` =  ?");
			$getPrereqs->execute(array($row2['ID']));
			$output['prereqs'] = array();
			while($row3 = $getPrereqs->fetch(PDO::FETCH_ASSOC)){
				//get the names of the pre-req etchs
				$getTechName = $db->prepare("SELECT `name` FROM `gameTechnology` WHERE `ID` = ?");
				$getTechName->execute(array($row3['techID'])) or die(var_dump($db->errorInfo()));
				$row4 = $getTechName->fetch(PDO::FETCH_ASSOC);
				array_push($output['prereqs'],$row4['name']);
			}
			$output['resources'] = getTechResearchCost($row['techID'],$nextLevel);
			array_push($final,$output);
		}else{

		}
	}
	return $final;
}


function getTechResearchCost($baseTechID,$level){
	include 'cookout.php';
	$getBaseCost = $db->prepare("SELECT `base_ore`,`base_crystal`,`base_hydrogen` FROM `gameBaseTechnologies` WHERE `ID` = ?");
	$getBaseCost->execute(array($baseTechID)) or die(var_dump($db->errorInfo()));
	$row = $getBaseCost->fetch(PDO::FETCH_ASSOC);
	$output = array();
	$output['ore'] = $row['base_ore'] * $level;
	$output['crystal'] =  $row['base_crystal'] * $level;
	$output['hydrogen'] = $row['base_hydrogen'] * $level;
	$output['time'] = ($output['ore'] + $output['hydrogen'] + $output['crystal']);
	return $output;
}
function getTechResourceRequirements($techID){
	include 'cookout.php';
	$getTechInfo = $db->prepare("SELECT `baseTech`,`baseTechLevel` FROM `gameTechnology` WHERE `ID` = ?");
	$getTechInfo->execute(array($techID)) or die(var_dump($db->errorInfo()));
	$row = $getTechInfo->fetch(PDO::FETCH_ASSOC);
	$getBaseTechInfo = $db->prepare("SELECT * FROM `gameBaseTechnologies` WHERE `ID` = ?");
	$getBaseTechInfo->execute(array($row['baseTech'])) or die(var_dump($db->errorInfo()));
	$rowBase = $getBaseTechInfo->fetch(PDO::FETCH_ASSOC);
	$final = array();
	$final['ore'] = $rowBase['base_ore'] * $row['baseTechLevel'];
	$final['crystal'] = $rowBase['base_crystal'] * $row['baseTechLevel'];
	$final['hydrogen'] = $rowBase['base_hydrogen'] * $row['baseTechLevel'];
	$final['time'] = $final['ore'] + $final['crystal'] + $final['hydrogen'];
	return $final;


}

function startResearch($techID){
	include 'cookout.php';
	//check and make sure that the tech hans't already been researched by the empire
	//get the new tech baseTechLevel and make sure that it's +1 of the current baseTechLevel
	$getBaseTech = $db->prepare("SELECT `baseTech`,`baseTechLevel` FROM `gameTechnology` WHERE `ID` = ?");
	$getBaseTech->execute(array($techID)) or die(var_dump($db->errorInfo()));
	$rowBaseTech = $getBaseTech->fetch(PDO::FETCH_ASSOC);
	$getCurrentBaseTechLevel = $db->prepare("SELECT `level` FROM `empireTechs` WHERE `userID` = ? AND `techID` = ?");
	$getCurrentBaseTechLevel->execute(array($_SESSION['UID'],$rowBaseTech['baseTech'])) or die(var_dump($db->errorInfo()));
	$currentLevelRow = $getCurrentBaseTechLevel->fetch(PDO::FETCH_ASSOC);

	if(!(($currentLevelRow['level'] + 1) == ($rowBaseTech['baseTechLevel']))){
		return "Your tech level is too low to do this research. Current level = ".$currentLevelRow['level']." Target Level = ".$rowBaseTech['baseTechLevel'];
	}
	//check and make sure that the tech is able to be researched by the empire

	/* wait...that should already be taken care of in the block above, right?? hm. */

	//check and make sure that the planet has enough resources
	$resources = getTechResourceRequirements($techID);
	$getAvailableResources = $db->prepare("SELECT `available_Ore`,`available_Crystal`,`available_Hydrogen` FROM `planets` WHERE `ID` = ?");
	$getAvailableResources->execute(array($_SESSION['activePlanet'])) or die(var_dump($db->errorInfo()));
	$availableResourcesRow = $getAvailableResources->fetch(PDO::FETCH_ASSOC);
	if($availableResourcesRow['available_Ore'] < $resources['ore']){
		return "You do not have enough ore";
	}
	if($availableResourcesRow['available_Crystal'] < $resources['crystal']){
		return "You do not have enough crystal";
	}
	if($availableResourcesRow['available_Hydrogen'] < $resources['hydrogen']){
		return "You do not have enough hydrogen";
	}
	//commit the research
	#pull the resources
	$pullResources = "UPDATE `planets` SET `available_Ore` = `available_Ore` - ".$resources['ore'].", `available_Crystal` = `available_Crystal` + ".$resources['crystal'].", `available_Hydrogen` = `available_Hydrogen` + ".$resources['hydrogen'];
	#insert the research row
	$insertResearchRow = "INSERT INTO `activeResearch` (`userID`,`baseTechID`,`toLevel`,`startTime`,`endTime`,`operatingPlanets`) VALUES (?,?,?,NOW(),NOW() + INTERVAL ".$resources['time']." SECOND,?)";
	$insertResearchRowArray = array();
	array_push($insertResearchRowArray,$_SESSION['UID'],$rowBaseTech['baseTech'],$rowBaseTech['baseTechLevel'],$_SESSION[activePlanet]);
	$pullResourcesQuery = $db->prepare($pullResources);
	$insertResearchRowQuery = $db->prepare($insertResearchRow);
	$pullResourcesQuery->execute(array()) or die(var_dump($db->errorInfo()));
	$insertResearchRowQuery->execute($insertResearchRowArray) or die(var_dump($db->errorInfo()));
	return "Research Started";

}

function generateProgressBar($start,$end,$title,$actionButton = null){
	//get total time needed on project
	$start = strtotime($start);
	$end = strtotime($end);
	$now = time();
	$totalTime = $end - $start;
	$timePassed = $now - $start;
	$timeLeft = $end - $now;
	$percentageComplete = ($timePassed / $totalTime) * 100;
	$percentageComplete = floor($percentageComplete);
	$timeLeft = secondsLeftIntoTime($timeLeft);

	echo "<li>\n";
	echo $title;
	if(!is_null($actionButton)){
		echo "<a href='".$actionButton['link']."' class='btn btn-mini pull-right ".$actionButton['classes']."'>".$actionButton['text']."</a>\n<br>\n";
	}
	echo "<div class='progress-bar-outer'>\n";
	echo "<span>".$percentageComplete."% ($timeLeft)</span>\n";
	echo "<div class='progress-bar-inner' style='width:".$percentageComplete."%'>\n";
	echo "<div class='progress-bar-background'></div>\n";
	echo "</li>\n";
}

function getTechInfoFromBaseTechAndLevel($baseTech,$baseTechLevel,$getFullInfo = false){
	/*
		If "getFullInfo" is true, then it will output the entire row, if it's true, then it will only get the ID of the row.
		This was placed to make the searches a lil bit faster
	*/
	include 'cookout.php';
	if($getFullInfo = false){
		$selector = '`ID`';
	}else{
		$selector = "*";
	}
	$getInfo = $db->prepare("SELECT $selector FROM `gameTechnology` WHERE `baseTech` = ? AND `baseTechLevel` = ?");
	$getInfo->execute(array($baseTech,$baseTechLevel)) or die(var_dump($db->errorInfo()));
	$row = $getInfo->fetch(PDO::FETCH_ASSOC);
	return $row;
}

function updateResearch($userID){
	include 'cookout.php';
	$getCompletedResearch = $db->prepare("SELECT * FROM `activeResearch` WHERE userID = ? AND `endTime` < NOW()");
	$getCompletedResearch->execute(array($_SESSION['UID'])) or die(var_dump($db->errorInfo()));
	while($row = $getCompletedResearch->fetch(PDO::FETCH_ASSOC)){
		//update
		$updateEmpireResearch = $db->prepare("UPDATE `empireTechs` SET `level` = ".$row['toLevel']." WHERE `techID` = ? AND `userID` = ?");
		$updateEmpireResearch->execute(array($row['baseTechID'],$_SESSION['UID']));
		//delete
		$deleteActiveResearchRow = $db->prepare("DELETE FROM `activeResearch` WHERE `ID` = ?");
		$deleteActiveResearchRow->execute(array($row['ID'])) or die(var_dump($db->errorInfo()));
	}
}

function getAvailableMissions($userID,$planetID){
	include 'cookout.php';
	$getMissionIDs = $db->prepare("SELECT `ID` FROM `gameMissions`");
	$getMissionIDs->execute(array()) or die(var_dump($db->errorInfo()));
	$final = array();
	while($row = $getMissionIDs->fetch(PDO::FETCH_ASSOC)){
		if(missionAvailableCheck($userID,$planetID,$row['ID'])){
			//mission is available for the user to use
			$getMissionInfo = $db->prepare("SELECT * FROM `gameMissions` WHERE `ID` = ?");
			$getMissionInfo->execute(array($row['ID'])) or die(var_dump($db->errorInfo()));
			$missionInfoRow = $getMissionInfo->fetch(PDO::FETCH_ASSOC);
			##TODO: write the information to the array
			$output = array();
			$output['ID'] = $missionInfoRow['ID'];
			$output['name'] = $missionInfoRow['name'];
			$output['description'] = $missionInfoRow['description'];
			$output['resources'] = array();
			$output['resources']['ore'] = $missionInfoRow['reward_ore'];
			$output['resources']['crystal'] = $missionInfoRow['reward_crystal'];
			$output['resources']['hydrogen'] = $missionInfoRow['reward_hydrogen'];
			$output['resources']['time'] = $missionInfoRow['missionTime'];
			$output['requirements'] = getMissionRequirementList($missionInfoRow['ID']);
			array_push($final,$output);
		}
	}
	return $final;
}

function getMissionRequirementList($missionID){
	include 'cookout.php';
	$final = array();
	$getMissionRequirements = $db->prepare("SELECT `requirementType`,`requirementNumber`,`requirementID` FROM `gameMissionRequirements` WHERE `missionID` = ?");
	$getMissionRequirements->execute(array($missionID));
	while($row = $getMissionRequirements->fetch(PDO::FETCH_ASSOC)){
		switch($row['requirementType']){
			case "ship":
				$name = getShipNameFromGameTechTable($row['requirementID']);
				break;
			case "tech":
				$name = getTechNameFromGameTechTable($row['requirementID']);
				break;
			case "building":
				$name = getBuildingNameFromGameTechTable($row['requirementID']);
				break;
		}
		$output = array();
		$output['number'] = $row['requirementNumber'];
		$output['name'] = $name;
		array_push($final,$output);
	}
	return $final;
}

function getTechNameFromGameTechTable($techID){
	include 'cookout.php';
	$getTechName = $db->prepare("SELECT `name` FROM `gameTechnolgy` WHERE `ID` = ?");
	$getTechName->execute(array($techID)) or die(var_dump($db->arrayInfo()));
	$row = $getTechName->fetch(PDO::FETCH_ASSOC);
	return $row['name'];
}

function getBuildingNameFromGameTechTable($techID){
	$getBuildingName = $db->prepare("SELECT `name` FROM `gameTechnolgy` WHERE `ID` = ?");
	$getBuildingName->execute(array($techID)) or die(var_dump($db->arrayInfo()));
	$row = $getBuildingName->fetch(PDO::FETCH_ASSOC);
	return $row['name'];
}

function getShipNameFromGameTechTable($techID,$type = null){
	include 'cookout.php';
	$getShipName = $db->prepare("SELECT `name` FROM `gameTechnology` WHERE `ID` = ?");
	$getShipName->execute(array($techID)) or die(var_dump($db->errorInfo()));
	$rowShipName = $getShipName->fetch(PDO::FETCH_ASSOC);
	
	//name of the ship is of the form of "[ship] class", so it needs to be chopped up a bit

	$shipNameArray = explode(" ",$rowShipName['name']);
	$shipName = $shipNameArray[0];

	switch($type){
		case "query":
			$shipName = "ships_".$shipName;
			break;
	}
	return $shipName;
}

function missionAvailableCheck($userID,$planetID,$missionID){
	include 'cookout.php';
	$getMissionRequirements = $db->prepare("SELECT * FROM `gameMissionRequirements` WHERE `missionID` = ?");
	$getMissionRequirements->execute(array($missionID)) or die(var_dump($db->errorInfo()));
	while($row = $getMissionRequirements->fetch(PDO::FETCH_ASSOC)){
		switch($row['requirementType']){
			case "ship":
				//check and see if the ship of that number class exists in the proper amount on the planet 

				$getNumberOfShipsParked = $db->prepare("SELECT `number` FROM `parkedShips` WHERE `shipID` = ? AND `planetID` = ?");
				$getNumberOfShipsParked->execute(array($row['requirementID'],$planetID)) or die(var_dump($db->errorInfo()));
				$rowNumberOfShips = $getNumberOfShipsParked->fetch(PDO::FETCH_ASSOC);
				if($row['requirementNumber'] > $rowNumberOfShips['number']){
					//there are more ships of the requirement than there are parked at the planet
					return false;
				}
				break;
			case "tech":
				$checkTechLevel = $db->prepare("SELECT `level` FROM `empireTechs` WHERE `userID` = ? AND `techID` = ?");
				$checkTechLevel->execute(array($userID,$row['requirementID'])) or die(var_dump($db->errorInfo()));
				$num = $checkTechLevel->rowCount();
				if($num = 0){
					//that means that the baseTech level isn't listed, which means that the tech level hasn't been reached
					return false;
				}
				$rowTechLevel = $checkTechLevel->fetch(PDO::FETCH_ASSOC);
				if($rowTechLevel)
				
				if($rowTechLevel['level'] < $row['requirementNumber']){
					//the current baseTech level is lower than the requirement baseTech level
					return false;
				}
				break;
			case "building":
				$checkBuildingStatus = $db->prepare("SELECT * FROM `planetBuildings` WHERE `planetID` = ? and `buildingID` = ?");
				$checkBuildingStatus->execute(array($planetID,$row['requirementID'])) or die(var_dump($db->errorInfo()));
				$num = $checkBuildingStatus->rowCount();
				if($num = 0){
					//building isn't built, requirement not met
					return false;
				}
				$rowBuildingInfo = $checkBuildingStatus->fetch(PDO::FETCH_ASSOC);
				if($rowbuildingInfo['number'] < $row['requirementNumber']){
					//there arent enough of the buildings (or modules)
					return false;
				}
		}
	}
	return true;
}

function startMission($userID,$planetID,$missionID){
	include 'cookout.php';
	if(missionAvailableCheck($userID,$planetID,$missionID)){
		//pull ships if necessary
		$getShipPullRequirements = $db->prepare("SELECT * FROM `gameMissionRequirements` WHERE `missionID` = ? ");
		$getShipPullRequirements->execute(array($missionID));
		while($row = $getShipPullRequirements->fetch(PDO::FETCH_ASSOC)){
			$shipClass = getShipNameFromGameTechTable($row['requirementID'],"query");
			$pullShips = $db->prepare("UPDATE `planets` SET $shipClass = $shipClass - ".$row['requirementNumber']." WHERE `ID` = ?");
			$pullShips->execute(array($planetID)) or die(var_dump($db->errorInfo()));
		}
		$getMissionTime = $db->prepare("SELECT `missionTime` FROM `gameMissions` WHERE `ID` = ?");
		$getMissionTime->execute(array($missionID)) or die(var_dump($db->errorInfo()));
		$rowGetMissionTime = $getMissionTime->fetch(PDO::FETCH_ASSOC);

		$addMissionToDatabase = $db->prepare("INSERT INTO `activeMissions` (`userID`,`missionID`,`planetID`,`startTime`,`endTime`,`multiplier`) VALUES (?,?,?,NOW(),NOW() + INTERVAL ".$rowGetMissionTime['missionTime']." SECOND,?)");
		$addMissionToDatabase->execute(array($userID,$missionID,$planetID,1)) or die(var_dump($db->errorInfo()));
		return "Misson Launched";		

	}else{
		return "Mission requirements are not met";
	}
}

function updatePlanetMissions($planetID){
	include 'cookout.php';
	$checkForCompletedMissions = $db->prepare("SELECT * FROM `activeMissions` WHERE `endTime` < NOW() AND `planetID` = ?");
	$checkForCompletedMissions->execute(array($planetID));
	while($row = $checkForCompletedMissions->fetch(PDO::FETCH_ASSOC)){
		//complete the mission

		//put available ships back
		moveShipsForMission($planetID,$row['missionID'],"add");
		//give the reward
		creditMissionReward($planetID,$row['missionID'],$row['multipler']);
		//delete the row
		$deleteRow = $db->prepare("DELETE FROM `activeMissions` WHERE `ID` = ?");
		$deleteRow->execute(array($row['ID'])) or die(var_dump($db->errorInfo()));
	}
}

function creditMissionReward($planetID,$missionID,$multipler){
	include 'cookout.php';
	$getResourceAmounts = $db->prepare("SELECT `reward_ore`,`reward_hydrogen`,`reward_crystal` FROM `gameMissions` WHERE `ID` = ?");
	$getResourceAmounts->execute(array($missionID)) or die(var_dump($db->errorInfo()));
	$resourceRow = $getResourceAmounts->fetch(PDO::FETCH_ASSOC);
	
	//update the resource amounts
	$updateResources = $db->prepare("UPDATE `planets` SET `available_Ore` = `available_Ore` + ".$resourceRow['reward_ore'].", `available_Crystal` = `available_Crystal` + ".$resourceRow['reward_crystal'].", `available_Hydrogen` = `available_Hydrogen` + ".$resourceRow['reward_hydrogen']." WHERE `ID` = ?");
	$updateResources->execute(array($planetID)) or die(var_dump($db->errorInfo()));
}

function moveShipsForMission($planetID,$missionID,$action){
	include 'cookout.php';
	$getRequirements = $db->prepare("SELECT `requirementID`,`requirementNumber` FROM `gameMissionRequirements` WHERE `requirementType` = 'ship' AND `missionID` = ?");
	$getRequirements->execute(array($missionID)) or die(var_dump($db->errorInfo()));
	while($row = $getRequirements->fetch(PDO::FETCH_ASSOC)){
		$shipName = getShipNameFromGameTechTable($row['requirementID'],"query");
		switch($action){
			case "add":
				$statement = "$shipName = $shipName + ".$row['requirementNumber'];
				break;
			case "remove":
				$statement = "$shipName = $shipName - ".$row['requirementNumber'];
				break;
		}
		$updateShips = $db->prepare("UPDATE `planets` SET $statement WHERE `ID` = ?");
		$updateShips->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	}
}

function getPlanetBuildingInfo($planetID){
	include 'cookout.php';
	$getBuildings = $db->prepare("SELECT * FROM `planetBuildings` WHERE `planetID` = ? AND moduleOf is NULL AND `superstructure` = 'yes'");
	$getBuildings->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	$final = array();
	while($buildingRow = $getBuildings->fetch(PDO::FETCH_ASSOC)){
		//go through every superstructure availble
		$superstructureInfo = getPlanetBuildingStats(true,$buildingRow['ID']);
		$superstructureInfo['subStructures'] = array();
		$getSubStructures = $db->prepare("SELECT * FROM `planetBuildings` WHERE `moduleOf` = ?");
		$getSubStructures->execute(array($buildingRow['ID']));
		while($subStructureRow = $getSubStructures->fetch(PDO::FETCH_ASSOC)){
			$moduleInfo = getPlanetBuildingStats(false,$subStructureRow['ID']);
			array_push($superstructureInfo['subStructures'],$moduleInfo);
		}
		array_push($final,$superstructureInfo);
	}
	return $final;
}

function getPlanetBuildingStats($superstructure,$planetBuildingID){
	$final = array();
	$final['ID'] = $planetBuildingID;
	$final['production'] = array();
	$final['consumption'] = array();
	$final['modules'] = array();
	#final['buildCost'] //only put on if unit is not a super
	//ssProcess handles the consumption and production values of the buildings
	$ssProcess = function($type,$process,$amount){
		$push = array();
		$push['type'] = $type;
		$push['amount'] = $amount;
		return $push;
	};

	$buildCostProcess = function($type,$amount){
		$push = array();
		$push['type'] = $type;
		$push['amount'] = $amount;
		return $push;
	};
	include 'cookout.php';
	//getPlanetbuildingID
	$getPlanetBuildingID = $db->prepare("SELECT * FROM `planetBuildings` WHERE `ID` = ?");
	$getPlanetBuildingID->execute(array($planetBuildingID)) or die(var_dump($db->errorInfo()));
	$planetBuildingRow = $getPlanetBuildingID->fetch(PDO::FETCH_ASSOC);
	//get game Building Info
	$getGameBuildingInfo = $db->prepare("SELECT `name`,`description` FROM `gameBuildings` WHERE `ID` = ?");
	$getGameBuildingInfo->execute(array($planetBuildingRow['buildingID'])) or die(var_dump($db->errorInfo()));
	$gameBuildingRow = $getGameBuildingInfo->fetch(PDO::FETCH_ASSOC);
	$final['name'] = $gameBuildingRow['name'];
	$final['description'] = $gameBuildingRow['description'];
	//get planet building production info
	$getProductionInfo = $db->prepare("SELECT * FROM `planetBuildingAttributes` WHERE `planetBuildingID` = ?");
	$getProductionInfo->execute(array($planetBuildingID)) or fail();
	while($productionInfoRow = $getProductionInfo->fetch(PDO::FETCH_ASSOC)){
		$push = array();
		$push['type'] = $productionInfoRow['type'];
		$push['amount'] = $productionInfoRow['amount'];
		array_push($final[$productionInfoRow['changeType']],$push);
	}
		//add the costs of the construction to the array
		if($planetBuildingRow['superstructure'] == 'no'){
			$final['buildCost'] = array(); //initialzing the array, only needed if not a super
			$final['productionModification'] = array();
			$final['consumptionModification'] = array();
			//but only if we're calculating a module
			//(the game is programmed right now to only allow modules to be buildable)
			$getConstructionCosts = $db->prepare("SELECT * FROM `gameBuildings` WHERE `ID` = ?");
			$getConstructionCosts->execute(array($planetBuildingRow['buildingID']));
			while($costRow = $getConstructionCosts->fetch(PDO::FETCH_ASSOC)){
				foreach($costRow as $key=>$value){
					if(isset($value)){ //gotta make sure that the only ones that process are rows that actually have numbers
						$split = explode("_",$key);
						if($split[3] == 'cost'){
							$nextNumber = $planetBuildingRow['amount'] + 1;
							$push = array();
							$push['type'] = $split[1];
							$push['amount'] = ($nextNumber * $value);
							array_push($final['buildCost'],$push);						
						}
						switch($split[1]){
							case "production":
								if($split[2] == "amount"){
									$push = array();
									$push['type'] = $split[0];
									$colName0 = $split[0]."_production_amount";
									$colName1 = $split[0]."_production_rate";
									$push['amount'] = $costRow[$colName0];
									$push['amount'] = "+ ".$push['amount']."/".$costRow[$colName1];
									array_push($final['productionModification'],$push);
								}								
							break;
							case "consumption":
								if($split[2] == "amount"){
									$push = array();
									$push['type'] = $split[0];
									$colName0 = $split[0]."_consumption_amount";
									$colName1 = $split[0]."_consumption_rate";
									$push['amount'] = $costRow[$colName0];
									$push['amount'] = "- ".$push['amount']."/".$costRow[$colName1];
									array_push($final['consumptionModification'],$push);
								}
							break;
						}
					}
					
				}
			}
			//calculate the build time based on the build cost
			$buildTime = 0;
			foreach($final['buildCost'] as $value){
				$buildTime = ($buildTime + $value['amount']);
			}
			$push = array(
				'type'=>'time',
				'amount'=>$buildTime);
			array_push($final['buildCost'],$push);
			}
/*	foreach($planetBuildingRow as $key=>$value){
		//echo "<br>$key:$value<br>";
		//go through each column and sort it accordingly
		$split = explode("_",$key);
		//collect consumption and production values
		switch($split[1]){
			case "production":
				if(isset($value)){
					$push = $ssProcess($split[0],"production",$value);
					array_push($final['production'],$push);
				}
				break;
			case "consumption":
				if(isset($value)){
					$push = $ssProcess($split[0],"consumption",$value);
					array_push($final['consumption'],$push);
				}
				

		}		
		//calculate the build time, which is the addition of all the resources needed. 1 sec per resource unit, regardless of type
	}
*/

	//get the info on modules
	$getModules = $db->prepare("SELECT `ID` FROM `planetBuildings` WHERE `moduleOf` = ?");
	$getModules->execute(array($planetBuildingRow['ID'])) or die(var_dump($db->errorInfo()));
	while($moduleRow = $getModules->fetch(PDO::FETCH_ASSOC)){
		$modInfo = getPlanetBuildingStats(false,$moduleRow['ID']);
		array_push($final['modules'],$modInfo);
	}
	return $final;
}

function startConstruction($planetBuildingID){
	include 'cookout.php';
	//see what planet the building is on
	$getPlanetID = $db->prepare("SELECT `planetID`,`operating`,`number` FROM `planetBuildings` WHERE `ID` = ?");
	$getPlanetID->execute(array($planetBuildingID)) or die(var_dump($db->errorInfo()));
	$planetIDRow = $getPlanetID->fetch(PDO::FETCH_ASSOC);
	//check for available resources
	$getAvailableResources = $db->prepare("SELECT `available_Ore`,`available_Crystal`,`available_Hydrogen` FROM `planets` WHERE `ID` = ?");
	$getAvailableResources->execute(array($planetIDRow['planetID'])) or die(var_dump($db->errorInfo()));
	$availableResourcesRow = $getAvailableResources->fetch(PDO::FETCH_ASSOC);
	$constructionCosts = getConstructionCosts($planetBuildingID);
	if($constructionCosts['ore'] > $availableResourcesRow['available_Ore']){
		return "Not enough ore";
	}
	if($constructionCosts['crystal'] > $availableResourcesRow['available_Crystal']){
		return "Not enough crystal";
	}
	if($constructionCosts['hydrogen'] > $availableResourcesRow['available_Hydrogen']){
		return "Not enoguh hydrogen";
	}
	//check to see if it's operating
	if($planetIDRow['operating'] == "yes"){
		return "Building is operating";
	}
	//pull the resources
	changeResources($planetIDRow['planetID'],"pull",$constructionCosts['ore'],$constructionCosts['crystal'],$constructionCosts['hydrogen']);

	//start construction
	$nextNumber = $planetIDRow['number'] + 1;
	$addConstructionRow = $db->prepare("INSERT INTO `activeBuildingConstruction` (`planetBuildingID`,`toNumber`,`startTime`,`stopTime`,`planetID`) VALUES (?,?,NOW(),NOW() + INTERVAL ".$constructionCosts['time']." SECOND,?)");
	$addConstructionRow->execute(array($planetBuildingID,$nextNumber,$planetIDRow['planetID'])) or die(var_dump($db->errorInfo()));
	return "Construction Started";

	//set building to operating
	$changeOPstatus = $db->prepare("UPDATE `planetBuildings` SET `operating` = 'yes' WHERE `ID` = ?");
	$changeOPstatus->execute(array($planetBuildingID)) or die(var_dump($db->errorInfo()));
}

function getConstructionCosts($planetBuildingID){
	include 'cookout.php';
	$output = array();
	$getGameBuildingID = $db->prepare("SELECT `buildingID`,`number` FROM `planetBuildings` WHERE `ID` = ?");
	$getGameBuildingID->execute(array($planetBuildingID)) or die(var_dump($db->errorInfo()));
	$buildingIDRow = $getGameBuildingID->fetch(PDO::FETCH_ASSOC);
	$getConstructionCosts = $db->prepare("SELECT `base_ore_construction_cost`,`base_crystal_construction_cost`,`base_hydrogen_construction_cost` FROM `gameBuildings` WHERE `ID` = ?");
	$getConstructionCosts->execute(array($buildingIDRow['buildingID'])) or die(var_dump($db->errorInfo()));
	$constructionCostRow = $getConstructionCosts->fetch(PDO::FETCH_ASSOC);
	$number = $buildingIDRow['number'] + 1;
	$output['ore'] = $constructionCostRow['base_ore_construction_cost'] * $number;
	$output['crystal'] = $constructionCostRow['base_crystal_construction_cost'] * $number;
	$output['hydrogen'] = $constructionCostRow['base_hydrogen_construction_cost'] * $number;
	$output['time'] = $output['ore'] + $output['crystal'] + $output['hydrogen'];
	return $output;
}

function changeResources($planetID,$action,$ore,$crystal,$hydrogen){
	include 'cookout.php';
	switch($action){
		case "add":
			$sign = "+";
			break;
		case "remove":
		case "subtract":
		case "pull":
			$sign = "-";
			break;
	}
	$updateResources = $db->prepare("UPDATE `planets` SET `available_Ore` = `available_Ore` $sign $ore, `available_Crystal` = `available_Crystal` $sign $crystal, `available_Hydrogen` = `available_Hydrogen` $sign $hydrogen WHERE `ID` = ?");
	$updateResources->execute(array($planetID)) or die(var_dump($db->arrayInfo()));
}

function getCurrentConstructionProjects($planetID){
	include 'cookout.php';
	$final = array();
	$getProjects = $db->prepare("SELECT * FROM `activeBuildingConstruction` WHERE `planetID` = ?");
	$getProjects->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	while($row = $getProjects->fetch(PDO::FETCH_ASSOC)){
		$output = array();
		//get the name of the project
		$getGameID = $db->prepare("SELECT `buildingID` FROM `planetBuildings` WHERE `ID` = ?");
		$getGameID->execute(array($row['planetBuildingID'])) or die(var_dump($db->errorInfo()));
		$gameIDRow = $getGameID->fetch(PDO::FETCH_ASSOC);

		$getBuildingName = $db->prepare("SELECT `name` FROM `gameBuildings` WHERE `ID` = ?");
		$getBuildingName->execute(array($gameIDRow['buildingID'])) or die(var_dump($db->errorInfo()));
		$buildingNameRow = $getBuildingName->fetch(PDO::FETCH_ASSOC);

		//get upgrade info

		$output['name'] = $buildingNameRow['name'];
		$output['upgrade'] = array();
		$output['upgrade']['type'] = 'number';
		$output['upgrade']['number'] = $row['toNumber'];
		$output['start'] = $row['startTime'];
		$output['end'] = $row['stopTime'];
		$output['ID'] = $row['ID'];
		array_push($final,$output);
	}
	return $final;
}

function updatePlanetConstruction($planetID){
	include 'cookout.php';
	$getCompleteProjects = $db->prepare("SELECT `ID` FROM `activeBuildingConstruction` WHERE `stopTime` < NOW()");
	$getCompleteProjects->execute(array($planetID)) or die(var_dump($db->errorInfo()));
	while($row = $getCompleteProjects->fetch(PDO::FETCH_ASSOC)){
		completePlanetConstruction($row['ID']);	
	}
}

function updateSuperStructure($planetBuildingID){
	##update the superstructure of the affected substructure ($planetBuildingID)
	include 'cookout.php';
	$getSSInfo = $db->prepare("SELECT `moduleOf` FROM `planetBuildings` WHERE `ID` = ?");
	$getSSInfo->execute(array($planetBuildingID)) or die(var_dump($db->errorInfo()));
	$SSInfoRow = $getSSInfo->fetch(PDO::FETCH_ASSOC);

	if(isset($SSInfoRow['moduleOf'])){
		$superStructurePlanetID = $SSInfoRow['moduleOf'];
		//now that we have the ID of the superstructure, then updates can be made on the entire thing
		$getAllSubstructures = $db->prepare("SELECT `ID` FROM `planetBuildings` WHERE `moduleOf` = ?");
		$getAllSubstructures->execute(array($superStructurePlanetID)) or fail();
		$substructureIDs = array();
		while($subStructureRow = $getAllSubstructures->fetch(PDO::FETCH_ASSOC)){
			array_push($substructureIDs,$subStructureRow['ID']);
		}
		$IDString = implode(",",$substructureIDs);
		//now that list (array) of affected buildings is set, see which ones are amount changers, and which are modifiers
		$getAmountRows = $db->prepare("SELECT * FROM `gameBuildingAttributes` WHERE `rate` = 'hour' AND `gameBuildingID` IN ($IDString)"); //get only amount rows
		$getAmountRows->execute(array()) or fail();
		while($amountRow = $getAmountRows->fetch(PDO::FETCH_ASSOC)){
			//update Production Numbers for the superstructure

			//check to see if there is a field already for the two datatypes
			$checkForRow = $db->prepare("SELECT `ID` FROM `planetBuildingAttributes` WHERE `resource` = ? AND `changeType` = ?");
			$checkForRow->execute($amountRow['resource'],$amountRow['changeType']) or fail();
			$num = $checkForRow->rowCount();
			if($num > 0){
				//there is a row, UPDATE
				extract($amountRow);
				$updateRow = $db->prepare("UPDATE `planetBuildingAttributes` SET `amount` = `amount` + $amount WHERE `planetBuildingID` = $superStructurePlanetID AND `resource` = '$resource' AND `chagneType` = '$changeType'");
				$updateRow->execute(array()) or fail();
			}else{
				//there is no row, INSERT
				$insertRow = $db->prepare("INSERT INTO `planetBuildingAttributes` (`planetBuildingID`,`resource`,`changeType`,`amount`) VALUES (?,?,?,?)");
				$insertRow->execute(array($superStructurePlanetID,$resource,$changeType,$amount)) or fail();
			}
		}
		//modifiers are added last, so that all of the absolute values can be added. 
		$getModifierRows = $db->prepare("SELECT * FROM `gameBuildingAttributes` WHERE `rate` = 'modifier' AND `gameBuildingID` IN ($IDString)"); //get only the modifiers
		$getModifierRows->execute(array()) or fail();
		while($modifierRow = $getModifierRows->fetch(PDO::FETCH_ASSOC)){
			//update production numbers for superstructure

			//check to see if there is a field already for the two datatypes
			$checkForRow = $db->prepare("SELECT  `ID` FROM `planetBuildingAttributes` WHERE `resource` = ? AND `changeType` = ?");
			$checkForRow->execute($modifierRow['resource'],$modifierRow['changeType']) or fail();
			$num = $checkForRow->rowCount();
			if($num > 0){
				//there is a row, UPDATE
				extract($modifierRow);
				$updateRow = $db->prepare("UPDATE `planetBuildingAttributes` SET `amount` = `amount` * $amount WHERE `planetBuildingID` = $superStructurePlanetID AND `resource` = '$resource' AND `changeType` = '$changeType'");
				$updateRow->execute(array()) or fail();

			} //no need for an else...if there isn't a row, nothing to modify, so do nothing.

		}
	}

}

function completePlanetConstruction($rowID){
	include 'cookout.php';
	//bump project to the new number
	$getRowInfo = $db->prepare("SELECT `planetBuildingID`,`toNumber` FROM `activeBuildingConstruction` WHERE `ID` = ?");
	$getRowInfo->execute(array($rowID)) or fail();
	$buildRowInfo = $getRowInfo->fetch(PDO::FETCH_ASSOC);
	$planetBuildingID = $buildRowInfo['planetBuildingID'];
	$newNumber = $buildRowInfo['toNumber'];
	$addNumber = $db->prepare("UPDATE `planetBuildings` SET `number` = ? where `ID` = ?");
	$addNumber->execute(array($newNumber,$planetBuildingID)) or die(var_dump($db->errorInfo())); //new number set now

	updateSuperStructure($planetBuildingID); //updates the superstructure of the changed substructure
	die($planetBuildingID);
	//update building production numbers
	//search for production numbers
	$getGameBuildingID = $db->prepare("SELECT `buildingID` FROM `planetBuildings` WHERE `ID` = ?"); //get gameBuildingID
	$getGameBuildingID->execute(array($row['planetBuildingID'])) or die(var_dump($db->errorInfo()));

	$gameBuildingIDRow = $getGameBuildingID->fetch(PDO::FETCH_ASSOC);


	$getBaseInfo = $db->prepare("SELECT * FROM `gameBuildings` WHERE `ID` = ?");  //get gameBuildingInfo, from gameBuildingID
	$getBaseInfo->execute(array($gameBuildingIDRow['buildingID'])) or die(var_dump($db->errorInfo()));
	$baseInfoRow = $getBaseInfo->fetch(PDO::FETCH_ASSOC);
		//update production numbers



	//search for new foundations to build
	$searchForUnlocks = $db->prepare("SELECT * FROM `gameTechnologyRequirements` WHERE `requirementType` = 'building' AND `requirementID` = ?");
	$searchForUnlocks->execute(array($row['buildingID'])) or die(var_dump($db->errorInfo()));
	while($unlockRow = $searchForUnlocks->fetch(PDO::FETCH_ASSOC)){
		//check to see if the building is built yet
		$searchForBuilding = $db->prepare("SELECT `ID` FROM `planetBuildings` WHERE `buildingID` = ? AND `planetID` = ?");
		$searchForBuilding->execute(array($row['buildingID'],$planetID)) or die(var_dump($db->errorInfo()));
		$num = $searchForBuilding->rowCount();
		if($num = 0){
			//building hans't been built or foundationed yet
			//build foundation
			$buildFoundation = $db->prepare("INSERT INTO `planetBuildings` (`planetID`,`buildingID`,`number`), VALUES (?,?,?)");
			$buildFoundation->execute(array($planetID,$row['buildingID'],0)) or die(var_dump($db->errorInfo()));
		}
	}
	//delete row
	$deleteRow = $db->prepare("DELETE FROM `activeBuildingConstruction` WHERE `ID` = ?");
	$deleteRow->execute(array($row['ID'])) or die(var_dump($db->errorInfo()));	
}

function checkBuildableRobots($planetID){
	
}

function getTechCategories(){
	include 'cookout.php';
	$getCategories = $db->prepare("SELECT DISTINCT `type` FROM `gameTechnology`");
	$getCategories->execute(array()) or die(var_dump($db->errorInfo()));
	$push = array();
	while($categoryRow = $getCategories->fetch(PDO::FETCH_ASSOC)){
		array_push($push,$categoryRow['type']);
	}
	return $push;
}

function getEmpireTechInfo($userID){
	//get categories

	include 'cookout.php';
	$getCategories = $db->prepare("SELECT DISTINCT `type` FROM `gameTechnology`");
	$getCategories->execute(array()) or die(var_dump($db->errorInfo()));
	$final = array();
	while($categoryRow = $getCategories->fetch(PDO::FETCH_ASSOC)){
		$push = array();
		$push['name'] = $categoryRow['type'];
		$push['techs'] = getTechsFromType($categoryRow['type'],$userID);
		array_push($final,$push);		
	}
	return $final;
}

function getTechsFromType($type,$userID){
	include 'cookout.php';
	$final = array();
	$getGameTechInfo = $db->prepare("SELECT `name`,`description`,`ID` FROM `gameTechnology` WHERE `type` = ?");
	$getGameTechInfo->execute(array($type)) or die(var_dump($db->errorInfo()));
	while($gameTechInfoRow = $getGameTechInfo->fetch(PDO::FETCH_ASSOC)){
		//cycling through the different techs of the type
		$push = array();
		$push['name'] = $gameTechInfoRow['name'];
		$push['description'] = $gameTechInfoRow['description'];
		$push['preReqs'] = getTechPreReqs($gameTechInfoRow['ID']);
		$push['ID'] = $gameTechInfoRow['ID'];
		array_push($final,$push);
	}
	return $final;
}

function getTechPreReqs($gameTechID){
	include 'cookout.php';
	$final = array();
	//get the prereqs
	$getRequirements = $db->prepare("SELECT `techID` FROM `gameTechnologyRequirements` WHERE `requirementID` = ?");
	$getRequirements->execute(array($gameTechID)) or die(var_dump($db->errorInfo()));
	while($requirementRow = $getRequirements->fetch(PDO::FETCH_ASSOC)){
		$push = array();
		$getInfo = $db->prepare("SELECT `name`,`description`,`ID` FROM `gameTechnology` WHERE `ID` = ?");
		$getInfo->execute(array($requirementRow['techID'])) or die(var_dump($db->errorInfo()));
		$infoRow = $getInfo->fetch(PDO::FETCH_ASSOC);
		$push['name'] = $infoRow['name'];
		$push['description'] = $infoRow['description'];
		$push['ID'] = $infoRow['ID'];
		array_push($final,$push);
	}
	return $final;
}

function adminLevelCheck($userID,$level){
	include 'cookout.php';
	$checkStatus = $db->prepare("SELECT * FROM `administrators` WHERE `userID` = ? AND `adminLevel` = ?");
	$checkStatus->execute(array($userID,$level)) or die(var_dump($db->errorInfo()));
	$num = $checkStatus->rowCount();
	if($num == 0){
		//no rows, their admin level isn't in the system
		return false;
	}else{
		//returns a row, their admin level is in the system
		return true;
	}
}

function getBuildableShips($planetID){
	include 'cookout.php';
	$final = array();
	$getGameShips = $db->prepare("SELECT * FROM `shipTypes` WHERE (`vehicleType` = ? OR `vehicleType` = ?)");
	$getGameShips->execute(array("ship",)) or die(var_dump($db->errorInfo()));
	while($shipTypeRow = $getGameShips->fetch(PDO::FETCH_ASSOC)){
		//ship type ID is now available, fetch the info for each ship
		$pushType = array();
		$pushType['typeName'] = ucwords($shipTypeRow['name']);
		$pushType['typeCode'] = $shipTypeRow['letterCode'];
		$pushType['ships'] = array();

		$getShipClasses = $db->prepare("SELECT * FROM `shipClasses` WHERE `shipType` = ?");
		$getShipClasses->execute(array($shipTypeRow['ID'])) or die(var_dump($db->errorInfo()));
		while($shipClassRow = $getShipClasses->fetch(PDO::FETCH_ASSOC)){
			$pushClass = array();
			$pushClass['ID'] = $shipClassRow['ID'];
			$pushClass['name'] = $shipClassRow['name'];
			$pushClass['description'] = $shipClassRow['description'];
			$pushClass['code'] = $shipClassRow['code'];
			//get stats for the ship
			$pushClass['stats'] = getShipClassStatsArray($shipClassRow['ID']);
			$pushClass['maxBuildNumber'] = getShipMaxBuildNumber($shipClassRow['ID'],$planetID);
			if($pushClass['maxBuildNumber'] > 0){
				array_push($pushType['ships'],$pushClass);
			}			
		}
		$num = count($pushType['ships']);
		if($num > 0){
			array_push($final,$pushType);
		}
	}
	return $final;
}

function getShipClassStatsArray($shipID){
	include 'cookout.php';
	$getStats = $db->prepare("SELECT * FROM `shipClasses` WHERE `ID` = ?");
	$getStats->execute(array($shipID)) or die(var_dump($db->errorInfo()));
	$row = $getStats->fetch(PDO::FETCH_ASSOC);
	$final = array();
	foreach($row as $key=>$value){
		$statPush = array();
		$split = explode("base_",$key);
		if(isset($split[1])){
			//key has a base_ beginning, which means that it's a state
			$split = explode("_",$key); //split again to get the full name of the stat
			$name = "";
			foreach($split as $splitValue){ //go through the split to get the full name
				if($splitValue != $split[0]){ //make sure the "base" part isn't included
					$name = $name." ".$splitValue;
				}				
			}
			$statPush['name'] = $name;
			$statPush['value'] = $value;
			if($statPush['value'] > 0){
				array_push($final,$statPush);
			}
			
		}
	}
	return $final;
}

function getShipMaxBuildNumber($shipID,$planetID){
	include 'cookout.php';
	$getMaxBuildFromResource = function($shipID,$resource,$planetID){
		include 'cookout.php';
		$resource = strtolower($resource);
		$fieldString = "base_construction_cost_".$resource;
		$getResourceBuildCost = $db->prepare("SELECT `$fieldString` FROM `shipClasses` WHERE `ID` = ?");
		$getResourceBuildCost->execute(array($shipID)) or fail();
		$resourceBuildCostRow = $getResourceBuildCost->fetch(PDO::FETCH_ASSOC);
		$shipBuildCost = $resourceBuildCostRow[$fieldString];
		if($shipBuildCost == 0){
			return null;
		}
		//got the ship build cost, now get the amount of the available resource on the planet

		$resource = ucfirst($resource);
		$fieldString = "available_".$resource;

		$getPlanetResourceAmount = $db->prepare("SELECT `$fieldString` FROM `planets` WHERE `ID` = ?");
		$getPlanetResourceAmount->execute(array($planetID)) or fail();
		$planetResourceAmountRow = $getPlanetResourceAmount->fetch(PDO::FETCH_ASSOC);

		$availableResource = $planetResourceAmountRow[$fieldString];
		$maxShips = $availableResource/$shipBuildCost; //will provide a decimal;
		$maxShips = floor($maxShips);
		return $maxShips;

	};
	//get Tech Requirements
	//checking on the tech requirements
	$canBuildShip = planetaryShipBuildingCheck($shipID,$planetID);
	if($canBuildShip == false){
		return 0;
	}
	//ship can be built, time to check to see how many of the ship can be built
	$resourceTypes = array("ore","crystal","hydrogen");
	$shipAmounts = array();
	foreach($resourceTypes as $value){
		$shipsFromResource = $getMaxBuildFromResource($shipID,$value,$planetID);		
		if($shipsFromResource != null){
			$shipAmounts[$value] = $shipsFromResource;
		}
	}
	sort($shipAmounts);
	return $shipAmounts[0];	
}



function planetaryShipBuildingCheck($shipID,$planetID){
	//you know it's a ship, so check for the different ship requirements
	include 'cookout.php';
	$getGameTechID = $db->prepare("SELECT `gameTechID` FROM `shipClasses` WHERE `ID` = ?");
	$getGameTechID->execute(array($shipID)) or die(var_dump($db->errorInfo()));
	$gameTechIDRow = $getGameTechID->fetch(PDO::FETCH_ASSOC);
	$shipTechID = $gameTechIDRow['gameTechID'];
	//ok we have the tech ID associated with the individual ship class

	$getTechRequirements = $db->prepare("SELECT * FROM `gameTechnologyRequirements` WHERE `techID` = ?");
	$getTechRequirements->execute(array($shipTechID)) or die(var_dump($db->errorInfo()));
	while($techRequirementRow = $getTechRequirements->fetch(PDO::FETCH_ASSOC)){
		$reqCheck = reqCheck($requirementID,$requirementType);
		if($reqCheck == false){
			return false;
		}
	}
	return true;
}

function reqCheck($requirementID,$type){
	include 'cookout.php';
	switch($type){
		case "tech":
			$getBaseInfo = $db->prepare("SELECT `baseTech`,`baseTechLevel` FROM `gameTechnology` WHERE `ID` = ?");
			$getBaseInfo->execute(array($db->errorInfo())) or die(var_dump($db->errorInfo()));
			$baseInfoRow = $getBaseInfo->fetch(PDO::FETCH_ASSOC);
			extract($baseInfoRow);
			//check the info for the specific empire
			$getEmpireTechInfo = $db->prepare("SELECT `techID`,`level` FROM `empireTechs` WHERE `userID` = ?");
			$getEmpireTechInfo->execute(array($_SESSION['UID'])) or die(var_dump($db->errorInfo()));
			$num = $getEmpireTechInfo->rowcount();
			if($num > 0){
				$empireTechInfoRow = $getEmpireTechInfo->fetch(PDO::FETCH_ASSOC);
				//row was found, empire has at least 0 (foundation [can research it])
				if($empireTechinfoRow['level'] >= $baseTechLevel){
					//empire tech lvel is either equal to or greater then the needed level
					return true;
				}else{
					//empire does not have requisite tech level
					return false;
				}
			}else{
				//row not found, empire hasn't even gotten far enough to be able to research the tech
				return false;
			}
		break;

	}
}

function addToShipBuildQueue($planetID,$shipClassID,$numberOfShips){
	include 'cookout.php';
	//make sure that the planet has enough resources to build the ships
	$getBuildCost = $db->prepare("SELECT `base_construction_cost_ore`,`base_construction_cost_crystal`,`base_construction_cost_hydrogen` FROM `shipClasses` WHERE `ID` =  ?");
	$getBuildCost->execute(array($shipClassID)) or fail();
	$buildCostRow = $getBuildCost->fetch(PDO::FETCH_ASSOC);
	//get the resource cost for the entire batch of ships. 
	$resources = array(
		"ore" => $buildCostRow['base_construction_cost_ore'] * $numberOfShips,
		"crystal" => $buildCostRow['base_construction_cost_crystal'] * $numberOfShips,
		"hydrogen" => $buildCostRow['base_construction_cost_hydrogen'] * $numberOfShips);

	//check and see if there is enough of each resource
	foreach($resources as $key => $value){
		$enoughResource = planetResourceAmountCheck($key,$value,$_SESSION['activePlanet']);
		if($enoughResource == false){
			return "There is not enough $key to build $numberOfShips ships";
		}
	}

	##insert the information into the build queue
	//check and see what the current queue number is
	$getHighestQueueNumber = $db->prepare("SELECT `queueNumber` FROM `activeShipConstruction` WHERE `planetID` = ?  ORDER BY `queueNumber` DESC LIMIT 1");
	$getHighestQueueNumber->execute(array($planetID)) or fail();
	$highestQueueNumberRow = $getHighestQueueNumber->fetch(PDO::FETCH_ASSOC);
	$num = $getHighestQueueNumber->rowCount();
	if($num == 0){
		$queueNumber = 0;
	}else{
		$queueNumber = $highestQueueNumberRow['queueNumber'] + 1;
	}

	//get the time for building each ship
	$shipBuildTime = $buildCostRow['base_construction_cost_ore'] + $buildCostRow['base_construction_cost_crystal'] + $buildCostRow['base_construction_cost_hydrogen'];

	//pull the resources
	changeResources($planetID,"add",$resources['ore'],$resources['crystal'],$resources['hydrogen']);

	//put the data into the row
	$insertBuildInfoIntoDB = $db->prepare("INSERT INTO `activeShipConstruction` (`shipClassID`,`planetID`,`secondsToBuildShip`,`lastUpdateTime`,`queueNumber`,`shipsLeftToBuild`) VALUES (?,?,?,NOW(),?,?)");
	$insertBuildInfoIntoDB->execute(array($shipClassID,$planetID,$shipBuildTime,$queueNumber,$numberOfShips)) or fail();

	return "Construction started for $numberOfShips ships";
}

function updatePlanetShipConstruction($planetID){
	include 'cookout.php';
	$getRows = $db->prepare("SELECT * FROM `activeShipConstruction` WHERE `planetID` = ? ORDER BY `queueNumber` ASC");
	$getRows->execute(array($planetID)) or fail();
	while($row = $getRows->fetch(PDO::FETCH_ASSOC)){
		if($row['shipsLeftToBuild'] == 0){
			$deleteRow = $db->prepare("DELETE FROM `activeShipConstruction` WHERE `ID` = ?");
			$deleteRow->execute(array($row['ID'])) or fail();
			updatePlanetConstruction($planetID);
		}else{
			$now = time();
			$lastUpdate = strtotime($row['lastUpdate']);
			$shipsLeft = $row['shipsLeftToBuild'];
			$timeDifference = $now - $lastUpdate;
			if($row['secondsToBuildShip'] > $timeDifference){
				return;
			}else{
				//see how many ships can be constructed in the time available
				$maxShips = $timeDifference / $row['secondsToBuildShip'];
				if($maxShips > $shipsLeft){
					//can build more ships than are left in the queue
					//construct the remaining ships, and then re-queue
					addShipsToPlanet($planetID,$row['shipClassID'],$row['shipsLeftToBuild']);
					$timePassed = $row['secondsToBuildShip'] * $shipsLeft;
					//delete the row, since ships have been added
					$deleteShipRow = $db->prepare("DELETE FROM `activeShipConstruction` WHERE `ID` = ?");
					$deleteShipRow->execute(array($row['ID'])) or fail();
					//update the time for all of the remaining rows, but only add the amount of time that has been used to build ships
					$updateTime = $db->prepare("UPDATE `activeShipConstruction` SET `lastUpdateTime` = `lastUpdateTime` + INTERVAL $timePassed SECOND WHERE `planetID` = ?");
					$updateTime->execute(array($planetID)) or fail();
					//recurse! 
					updatePlanetConstruction($planetID);
				}else{
					//can only build a limited amount of ships in with the available time.
					$shipsBuilt = $timeDifference / $row['secondsToBuildShip'];
					$shipsBuilt = floor($shipsBuilt);
					$timeSpentBuilding = $shipsBuilt * $row['secondsToBuildShip'];
					$timeRemaining = $timeDifference - $timeSpentBuilding;
					//add the ships to the planet
					addShipsToPlanet($planetID,$row['shipClassID'],$shipsBuilt);
					//update the ship count
					$update = $db->prepare("UPDATE 	`activeShipConstruction` SET `shipsLeftToBuild` = `shipsLeftToBuild` - $shipsBuilt WHERE `ID` = ?");
					$update->execute(array($row['ID'])) or fail();
					//update the time count
					$updateTime = $db->prepare("UPDATE `activeShipConstruction` SET `lastUpdateTime` = NOW - INTERVAL $timeRemaining SECOND WHERE `planetID` = ? ");
					$updateTime->execute(array($planetID)) or fail();
					//no need to recurse, this is the end game.
				}
			}
		}
	}
}



function planetResourceAmountCheck($resource,$requiredAmount,$planetID){
	include 'cookout.php';
	$resource = ucfirst($resource);
	$fieldString = "available_".$resource;
	$getResourceAmount = $db->prepare("SELECT `$fieldString` FROM `planets` WHERE `ID` = ?");
	$getResourceAmount->execute(array($planetID)) or fail();
	$resourceAmountRow = $getResourceAmount->fetch(PDO::FETCH_ASSOC);
	$amountAvailable = $resourceAmountRow[$fieldString];
	if($amountAvailable >= $requiredAmount){
		return true; //there is enough of the resource
	}else{
		return false; //there is not enough of the resource
	}
}

function setTributePrizeNumber($amount){
	function recalculateRarities(){
		include 'cookout.php';
		//recalculate the rarities inside the tributeInventory table

		//get all of the tribute rows
		$getAllRows = $db->prepare("SELECT * FROM `tributeInventory`");
		$getAllRows->execute(array()) or fail();
		//see what the highest rarity value is
		$highestVal = 0;
		while($row = $getAllRows->fetch(PDO::FETCH_ASSOC)){
			if($row['base_rarity'] > $highestVal){
				$highestVal = $row['base_rarity'];
			}
		}
		//go through and calculate the display rarity for each row, as well as assign spin values
		$spinNumber = 0;
		$getAllRows->execute(array()) or fail();
		while($row = $getAllRows->fetch(PDO::FETCH_ASSOC)){

			$inverse = 1 / $row['base_rarity'];
			$rarity = $highestVal * $inverse;
			$rarity = floor($rarity);
			$minStop = $spinNumber + 1;
			$maxStop = $spinNumber + $rarity;
			$spinNumber = $spinNumber + $rarity; //set it up for the next item in line
			$updateRarity = $db->prepare("UPDATE `tributeInventory` SET `display_rarity` = ?,`minStop` = ?,`maxStop` = ? WHERE `ID` = ?");
			$updateRarity->execute(array($rarity,$minStop,$maxStop,$row['ID'])) or fail();
		}
	};
	include 'cookout.php';
	//get the current number of items in the inventory
	$limit  = $amount + 1;
	$getNumber = $db->prepare("SELECT `ID` FROM `tributeInventory` LIMIT $limit");
	$getNumber->execute(array()) or fail();
	$num = $getNumber->rowCount();
	if($num > $amount){
		//there are more rows in the inventory than needed.

		//delete extra rows 
		$deletionsLeft = $num - $amount;
		$getRowsToDelete = $db->prepare("SELECT `ID` FROM `tributeInventory` ORDER BY RAND() LIMIT $deletionsLeft");
		$getRowsToDelete->execute(array()) or fail();
		while($deleteRow = $getRowsToDelete->fetch(PDO::FETCH_ASSOC)){
			$deleteRowQuery = $db->prepare("DELETE FROM `tributeInventory` WHERE `ID` = ?");
			$deleteRowQuery->execute(array($deleteRow['ID'])) or fail();
		}

	}elseif($num < $amount){
		//generate new rows
		$generationsLeft = $amount - $num;
		while($generationsLeft > 0){
			//generate new row

			//find random prize
			$getPrizeID = $db->prepare("SELECT * FROM `gameInventoryItems` ORDER BY RAND() LIMIT 1");
			$getPrizeID->execute(array()) or fail();
			$prizeIDRow = $getPrizeID->fetch(PDO::FETCH_ASSOC);
			//generate the item of the amount that is going to be given w/ the prize
			$prizeValue = rand(1,$prizeIDRow['itemAmountMax']);
			//get the percent of the max that is going to be given
			$prizeValuePercent = $prizeValue / $prizeIDRow['itemAmountMax'];
			//increase the amount of the rarity by the % of the prize being given
			$addOnRarity = $prizeIDRow['itemRarity'] * $prizeValuePercent;
			$addOnRarity = floor($addOnRarity);
			$finalRarity = $prizeIDRow['itemRarity'] + $addOnRarity;
			$insertRow = $db->prepare("INSERT INTO `tributeInventory` (`itemID`,`itemRewardAmount`,`base_rarity`) VALUES (?,?,?)");
			$insertRow->execute(array($prizeIDRow['ID'],$prizeValue,$finalRarity));
			$generationsLeft = $generationsLeft - 1;
		}
	}
	recalculateRarities();
}

function getTributePrizes(){
	include 'cookout.php';
	$getRows = $db->prepare("SELECT * FROM `tributeInventory`");
	$getRows->execute(array()) or fail();
	$final = array();
	while($row = $getRows->fetch(PDO::FETCH_ASSOC)){
		//get the name of and description
		$push = array();
		$getInfo = $db->prepare("SELECT `name`,`description` FROM `gameInventoryItems`  WHERE `ID` = ?");
		$getInfo->execute(array($row['itemID'])) or fail();
		$infoRow = $getInfo->fetch(PDO::FETCH_ASSOC);
		$push['name'] = $infoRow['name'];
		$push['description'] = $infoRow['description'];
		$push['amount'] = $row['itemRewardAmount'];
		$push['rollChance'] = $row['display_rarity'];
		array_push($final,$push);
	}
	return $final;
}

function spinTributeWheel($userID){
	include 'cookout.php';
	//check for number of tribute spins
	$getSpinsLeft = $db->prepare("SELECT `tributeSpins` FROM `users` WHERE `ID` = ?");
	$getSpinsLeft->execute(array($userID)) or fail();
	$spinsLeftRow = $getSpinsLeft->fetch(PDO::FETCH_ASSOC);
	if($spinsLeftRow['tributeSpins'] < 1){
		return "You do not have enough tribute spins.";
	}else{
		//user has enough tribute spins left, now spin da wheel!
		//get the highest number
		$getHighestNum = $db->prepare("SELECT `maxStop` FROM `tributeInventory` ORDER BY `maxStop` DESC LIMIT 1"); //get the highest maxstop number
		$getHighestNum->execute(array()) or fail();
		$highestNumRow = $getHighestNum->fetch(PDO::FETCH_ASSOC);
		$higestNum = $highestNumRow['maxStop'];
		$randomNumber = rand(1,$highestNum);
		$getPrize = $db->prepare("SELECT `ID` FROM `tributeInventory` WHERE ((`minStop` <= $randomNumber) AND `maxStop` >= $randomNumber)");
		$getPrize->execute(array()) or fail();
		$prizeRow = $getPrize->fetch(PDO::FETCH_ASSOC);
		awardItemToUser($userID,$prizeRow['ID']);
		return "You won a prize!";
	}

}

function awardItemToUser($userID,$prizeID,$prizeSource = "tribute"){
	include 'cookout.php';
	switch($prizeSource){
		case "store":
		//for future use
			break;
		case "tribute":
			//getInfo on the prize
			$getPrizeInfo = $db->prepare("SELECT * FROM `tributeInventory` WHERE `ID` = ?");
			$getPrizeInfo->execute(array($prizeID)) or fail();
			$prizeInfoRow = $getPrizeInfo->fetch(PDO::FETCH_ASSOC);
			//transfer the info to the user's inventory
			$insertUserInventory = $db->prepare("INSERT INTO `userInventory` (`userID`,`itemID`,`itemRewardAmount`,`source`) VALUES (?,?,?)");
			$insertUserInventory->execute(array($userID,$prizeInfoRow['itemID'],$prizeInfoRow['itemRewardAmount'],"tribute")) or fail();
			//remove the item from tribute Inventory
			$deleteRow = $db->prepare("DELETE FROM `tributeInventory` WHERE `ID` = ?");
			$deleteRow->execute(array($prizeInfoRow['ID'])) or fail();
			//get current number of tribute rows
			$getCurrentNumber = $db->prepare("SELECT `ID` FROM `tributeInventory`");
			$getCurrentNumber->execute(array()) or fail();
			$num = $getCurrentNumber->rowCount();
			//set number to current number + 1 to replace deleted item
			setTributePrizeNumber($num + 1);
	}
}

function getUserInventory($userID){
	include 'cookout.php';
	$final = array();
	$getInventory = $db->prepare("SELECT * FROM `userInventory` WHERE `userID` = ?");
	$getInventory->execute(array($userID)) or fail();
	while($row = $getInventory->fetch(PDO::FETCH_ASSOC)){
		//get info on the prize
		$getInfo = $db->prepare("SELECT `name`,`description` FROM `gameInventoryItems` WHERE `ID` = ?");
		$getInfo->execute(array($row['itemID'])) or fail();
		$infoRow = $getInfo->fetch(PDO::FETCH_ASSOC);
		$push['name'] = $infoRow['name'];
		$push['description'] = $infoRow['description'];
		$push['amount'] = $row['itemRewardAmount'];
		$push['ID'] = $row['ID'];
		array_push($final,$push);
	}
	return $final;
}

function useInventoryItem($userID,$planetID,$itemID){
	include 'cookout.php';
	//get the info from the user row, such as the prize type and the amount
	$getUserInventoryInfo = $db->prepare("SELECT * FROM `userInventory` WHERE `ID` = ?");
	$getUserInventoryInfo->execute(array($itemID)) or fail();
	$userInventoryRow = $getUserInventoryInfo->fetch(PDO::FETCH_ASSOC);
	//get the specific prize information
	switch($userInventoryRow['source']){
		case "tribute":
			$table = "`gameInventoryItems`";
			break;
		case "store":
			$table = "`storeInventoryItems`";
			break;
	}
	$getGamePrizeInfo = $db->prepare("SELECT * FROM $table WHERE `ID` = ?");
	$getGamePrizeInfo->execute(array($userInventoryRow['itemID'])) or fail();
	$gamePrizeInfoRow =  $getGamePrizeInfo->fetch(PDO::FETCH_ASSOC);
	switch($gamePrizeInfoRow['itemType']){
		case "ship":
			//add the ships to the planet
			addShipsToPlanet($planetID,$gamePrizeInfoRow['itemID'],$userInventoryRow['itemRewardAmount']);
			break;
		case "resource":
			//select which of the categories of resource need to be dumped into
			switch($gamePrizeInfoRow['itemID']){
				case "ore":
					changeResources($planetID,"add",$userInventoryRow['itemRewardAmount'],0,0);
					break;
				case "crystal":
					changeResources($planetID,"add",0,$userInventoryRow['itemRewardAmount'],0);
					break;
				case "hydrogen":
					changeResources($planetID,"add",0,0,$userInventoryRow['itemRewardAmount']);
					break;
			}

	}
	//delete the row
	$deleteRow = $db->prepare("DELETE FROM `userInventory` WHERE `ID` = ?");
	$deleteRow->execute(array($userInventoryRow['ID'])) or fail();
}

function getShopItems($userID){
	include 'cookout.php';
	$getCategories = $db->prepare("SELECT DISTINCT `itemType` FROM `storeInventoryItems`");
	$getCategories->execute(array()) or fail();
	$final = array();
	while($categoryRow = $getCategories->fetch(PDO::FETCH_ASSOC)){
		$categoryPush = array();
		$categoryPush['categoryName'] = $categoryRow['itemType'];
		$categoryPush['items'] = array();
		$getItems = $db->prepare("SELECT * FROM `storeInventoryItems` WHERE `itemType` = ?");
		$getItems->execute(array($categoryRow['itemType'])) or fail();
		while($itemRow = $getItems->fetch(PDO::FETCH_ASSOC)){
			$itemPush = array();
			$itemPush['ID'] = $itemRow['ID'];
			$itemPush['name'] = $itemRow['name'];
			$itemPush['description'] = $itemRow['description'];
			$itemPush['purchaseCost'] = $itemRow['creditCost'];
			$itemPush['itemAmount'] = $itemRow['itemAmountMax'];
			array_push($categoryPush['items'],$itemPush);
		}
		array_push($final,$categoryPush);
	}
	return $final;
}

function buyShopItem($userID,$itemID){
	include 'cookout.php';
	//get the item info
	$getInfo = $db->prepare("SELECT * FROM `storeInventoryItems` WHERE `ID` = ?");
	$getInfo->execute(array($itemID)) or fail();
	$itemInfoRow = $getInfo->fetch(PDO::FETCH_ASSOC);
	//check and see if the user has enough credits to buy the item
	$getCredits = $db->prepare("SELECT `credits` FROM `users` WHERE `ID` = ?");
	$getCredits->execute(array($userID)) or fail();
	$creditsRow = $getCredits->fetch(PDO::FETCH_ASSOC);
	if($itemInfoRow['creditCost'] > $creditsRow['credits']){
		//the user does not have enough credits.
		return "Not enough credits available to make this purchase";
	}else{
		//pull the credits from the user
		$pullCredits = $db->prepare("UPDATE `users` SET `credits` = `credits` - ".$itemInfoRow['creditCost']." WHERE `ID` = ?");
		$pullCredits->execute(array($userID)) or fail();
		//give the item to the user
		$addItem = $db->prepare("INSERT INTO `userInventory` (`userID`,`itemID`,`itemRewardAmount`,`source`) VALUES (?,?,?,?)");
		$addItem->execute(array($userID,$itemID,$itemInfoRow['itemAmountMax'],"store")) or fail();
		return "Item Purchased";
	}
	return "No action made.";
}

function fail(){
	die(var_dump($db->errorInfo));
}

