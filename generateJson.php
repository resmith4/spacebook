<?php
$nodes = array();
$links = array();

function addLink($source,$target,$links){
	$link = array("source"=>$source,"target"=>$target);
	array_push($links,$link);
}

function addNode($name,$type,$nodes){
	$node = array("name"=>$name,"type"=>$type);
	array_push($nodes,$node);
}

$techs = array(
	'Space',
	'Construction',
	"Communication",
	"Research",
	"Power",
	"Mining",
	"Fabrication",
	"Shipbuilding",
	"Rocketry",
	"Defense",
	"Robot",
	"Lasers",
	"Nuclear",
	"Pulse Drive",
	"Fusion",
	"Scanning",
	"Commerce",
	"Trade",
	"Missile",
	"Particle",
	"Warp Drive");
$ships=array(
"Apis Class",
"Culicidae Class",
"Vespula Class",
"Vespa Class",
"Hymenoptera Class",
"Lupus Class",
"Jamaicensis Class",
"Serpentes Class",
"Mississippiensis Class",
"Tigris Class",
"Leo Class",
"Gorilla Class",
"Capensis Class",
"Capra Class",
"Asinus Class",
"Caballus Class",
"Capra-P Variant",
"Primigenius Class",
"Caballus-P Variant",
"Elephantidae Class",
"Tarandus",
"Tarandus-P Variant",
"Macrorhynchos Class",
"Livia Class",
"Africanus Class",
"Strigiformes Class",
"Latrans Class",
"Maritimus Class",
"Macrorhynchos-S Variant"
);

$buildings=array(
	"Ore Power Plant",
	"Ore Power Plant (Power Module)",
	"Ore Power Plant (Efficiency Module)",
	"Ore Mine",
	"Ore Mine (Drill Module)",
	"Ore Mine (Power Conduit)",
	"Crystal Mine",
	"Crystal Mine (Drill Module)",
	"Crystal Mine (Power Conduit)",
	"Hydrogen Factory",
	"Hydrogen Factory (Production Module)",
	"Hydrogen Factory (Power Conduit)",
	"Base",
	"Bade (Admin Module)",
	"Base (Finance Department)",
	"Base (Trade Bureau)",
	"Research Lab",
	"Research Lab (Admin Module)",
	"Research Lab (Research Bay)",
	"Research Lab (Comm Array)",
	"Shipyard",
	"Shipyard (Fab Module)",
	"Shipyard (Construction Array)"
	);


foreach($techs as $value){
	$techCodes = array("I","II","III","IV","V","VI","VII","VIII","IX","X");
	for($x=0;$x<=10;$x++){
		$node = array(
		'name'=>$value." Tech ".$techCodes[$x],
		'type'=>'tech');
	array_push($nodes,$node);
		if($x<10 && $x>0){
			$link = array(
				'source'=>$value." Tech ".$techCodes[($x-1)],
				'target'=>$value." Tech ".$techCodes[$x]
				);
			array_push($links,$link);
		}
	}
	
}

foreach($ships as $value){
	$node = array(
		"name"=>$value,
		"type"=>"ship");
	array_push($nodes,$node);
}

foreach($buildings as $value){
	$node = array(
		"name"=>$value,
		"type"=>"building");
	array_push($nodes,$node);
}

//Add Custon Nodes

//Add Custom Links
addLink("Space Tech I","Ore Power Plant",$links);
addLink("Space Tech II", "Ore Mine",$links);
addLink("Space Tech III", "Crystal Mine",$links);

$final = array("nodes"=>$nodes,"links"=>$links);


//Adding Custom Links
if (($handle = fopen("customLinks.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        $link = array("source"=>$data[0],"target"=>$data[1]);
        array_push($links,$link);
    }
    fclose($handle);
}
echo "var nodes = ";
echo json_encode($nodes);
echo ";";
echo "<br>";
echo "var links = ";
echo json_encode($links);
echo ";";