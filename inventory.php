<?php session_start();
include 'cookout.php';
include_once 'functions.php';
include_once 'objects.php';

if($_SESSION['auth'] != 'yes'){
	include "spash_screen.php";die();
}
if($_GET['action'] == 'useItem'){
	$message = useInventoryItem($_SESSION['UID'],$_SESSION['activePlanet'],$_GET['itemID']);
}

updatePlanetResources($_SESSION['activePlanet']);
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Spacebook</title>
	<?php 	include 'linksAndScripts.php';
			include 'templates/floatingStars_JS.php';
	?>
</head>
<body>
	<?php
	include 'templates/floatingStars_HTML.php';
	$resources = echoAvailableResources($_SESSION['activePlanet']);
	echoPlanetLocation($_SESSION['activePlanet']);
	?>

	<div id="page-wrap">
		<div class='container-fluid'>
			<div class='row-fluid'>
				<?php include "templates/spacebookHeader.php" ?>
			</div>
			<div style='margin-top:15px' class='row-fluid'>
				<div class='span3'>
					<?php include "templates/infoSidebar.php" ?>
				</div>
				<div class='span9'>
					<?php echo $message ?>
					<?php include 'templates/inventoryInfo.php' //INFO SCREEN INCLUDE GOES HERE ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
