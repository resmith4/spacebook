<form class='form-horizontal' action='processLogin.php' method="POST">
	<div class='control-group'>
		<label class='control-label' for='username'>Username</label>
		<div class='controls'>
			<input type='text' name='username' id='username'>
		</div>
	</div>
	<div class='control-group'>
		<label class='control-label' for="password">Password</label>
		<div class='controls'>
			<input type='password' name='password' id='password'>
		</div>
	</div>
	<div class='control-group'>
		<div class='controls'>
			<input class='btn btn-primary' type='submit' value='Log In'>
		</div>
	</div>
</form>
