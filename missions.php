<?php session_starT();
include 'cookout.php';
include_once 'functions.php';
include_once 'objects.php';

if($_SESSION['auth'] != 'yes'){
	header("Location:spash_screen.php");die();
}

//updatePlanetResources($_SESSION['activePlanet']);
updatePlanetMissions($_SESSION['activePlanet']);

if($_GET['action'] == 'startMission'){
	$message = startMission($_SESSION['UID'],$_SESSION['activePlanet'],$_GET['missionID']);
}
?><!DOCTYPE HTML>
<html>
<head>
	<title>Spacebook | Missions</title>
	<?php 	include 'linksAndScripts.php';

	?>
</head>
<body>
	
	<div id="page-wrap">
		<div class='container-fluid'>
			<div class='row-fluid'>
				<?php include 'templates/spacebookHeader.php'; ?>
			</div>
			<div style='margin-top:15px' class='row-fluid'>
				<div class='span3'>
					<div class='planet-info-pane'>
						<?php include "templates/infoSidebar.php" ?>
					</div>
				</div>
				<div class='span9'>
					<?php echo $message; ?>
					<?php include 'templates/missionInfoPane.php'; ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>