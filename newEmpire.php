<?php session_start();
include 'cookout.php';
include_once 'functions.php';
include_once 'objects.php';

if($_SESSION['auth'] != 'yes'){
	include "spash_screen.php";die();
}
if($_POST['action'] == 'createEmpire'){
	//create empire
}

$fields = array();
array_push($fields,array("empireName","Empire Name","Empire Name description"));

updatePlanetResources($_SESSION['activePlanet']);
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Spacebook</title>
	<?php 	include 'linksAndScripts.php';
			include 'templates/floatingStars_JS.php';
	?>
</head>
<body>
	<?php
	include 'templates/floatingStars_HTML.php';
	$resources = echoAvailableResources($_SESSION['activePlanet']);
	echoPlanetLocation($_SESSION['activePlanet']);
	?>

	<div id="page-wrap">
		<div class='container-fluid'>
			<h1 class='center'>New Empire</h1>
			<p>Literary Paragraph</p>
			<div class='span9'>
				<?php foreach($fields as $value){ ?>

				<form class='form-horizontal'>
					<div class='control-group'>
						<label class='control-label' for='<?php echo $value[0] ?>'><?php echo $value[1] ?></label>
						<div class='controls'>
							<input type='text' id='<?php echo $value[0] ?>'>
						</div>
					</div>

				<?php } //closing up the foreach($fields as $value)?>

					<div class='control-group'>
						<div class='controls'>
							<input type='submit' value='Submit' id='submit' class='btn'>
						</div>
					</div>
					<input type='hidden' name='action' value='createEmpire'>
				</form>


				<?php //INFO SCREEN INCLUDE GOES HERE ?>
			</div>
		</div>
	</div>
</body>
</html>
