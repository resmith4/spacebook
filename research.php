<?php session_start();
include 'cookout.php';
include_once 'functions.php';
include_once 'objects.php';
//processResearch($_SESSION[activePlanet]);

if($_SESSION['auth'] != 'yes'){
	include "spash_screen.php";die();
}

updatePlanetResources($_SESSION['activePlanet']);
updateResearch($_SESSION['UID']);

if($_GET['action'] == "startResearch"){
	$message = startResearch($_GET['techID']);
}

?><!DOCTYPE HTML>
<html>
<head>
	<title>Spacebook | Research</title>
	<?php 	include 'linksAndScripts.php';
			include 'templates/floatingStars_JS.php';
	?>
</head>
<body>
	
	<div id="page-wrap">
		<div class='container-fluid'>
			<div class='row-fluid'>
				<?php include "templates/spacebookHeader.php" ?>
			</div>
			<div style='margin-top:15px' class='row-fluid'>
				<div class='span3'>
					<div class='planet-info-pane'>
						<?php include "templates/infoSidebar.php" ?>
					</div>
				</div>
				<div class='span9'>
					<?php if(!is_null($message)){echo $message;} ?>
					<?php include 'templates/activeResearchPane.php' ?>
					<?php include 'templates/researchInfoPane.php' ?>
				</div>
			</div>
		</div>
	</div>

</body>
</body>
</html>