<?php session_start();
include 'cookout.php';
include_once 'functions.php';
include_once 'objects.php';
if($_SESSION['auth'] != 'yes'){
	header("location:index.php");
	die();
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Spacebook</title>
	<?php include 'linksAndScripts.php'; ?>
</head>
<body>
	<div id="page-wrap">
		<div class='container-fluid'>
			<div class='row-fluid'>
				<?php include "templates/spacebookHeader.php" ?>
			</div>
			<div class='row-fluid'>
				<div class='span3'>
					<?php include "templates/infoSidebar.php" ?>
				</div>
				<div class='span9'>
					<?php include "templates/scanInfo.php" ?>			
				</div>




			</div>


		</div>
	</div>
</body>
</html>