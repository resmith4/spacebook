<!DOCTYPE html>
<html >

<head>
	
	<title>Spacebook | Class Selection</title>
	
	
	<?php include 'linksAndScripts.php' ?>
	<link rel='stylesheet' type='text/css' href='css/selectClass.css' />
  <script type='text/javascript' src='js/selectClass.js'></script>   
</head>

<body>

	<div class='page-wrap' id='col1'>
		<div class="info-col">
		
			<h2>Geologist</h2>
			
			<a class="image geologist">View Image</a>
			
			<dl>
				<dt>Bonuses</dt>
				<dd>
					<ol>
						<li>5% Mine Production</li>
					</ol>
				</dd>

				<dt>Strategy</dt>
				<dd>
					Mines are the passive way of gathering resources in the game. Players who would rather build mines, and then let the mines do the work, then this is the class for them.
				</dd>

				<dt>Story</dt>
				<dd>
					[Story about the Geologist]
				</dd>

				<dt>Select</dt>
				<dd>
					You are about to select the Geologist Class.<br><btn class='btn btn-primary'>Select <i class='icon-double-angle-right'></i></btn>
				</dd>
			</dl>       
		</div>

			
		  <div class="info-col">
		  
			<h2>Scientist</h2>
			
			<a class="image scientist">View Image</a>
			
			<dl>
				<dt>Bonuses</dt>
				<dd>
					<ol>
						<li>Research gets done 5% Faster</li>
					</ol>
				</dd>

				<dt>Strategy</dt>
				<dd>
					Research allows a player to unlock new buildings and new units. Players who want to fine-tune their empire, and do not want to be limited by the available units would be suited for this class
				</dd>

				<dt>Story</dt>
				<dd>
					[story about the scientist]
				</dd>

				<dt>Select</dt>
				<dd>
					You are about to select the Scientist Class.<br><btn class='btn btn-primary'>Select <i class='icon-double-angle-right'></i></btn>
				</dd>
			</dl>
		  
		  </div>
			
			<div class="info-col">
			
				<h2>General</h2>
				
				<a class="image general">View Image</a>
				
				<dl>
				<dt>Bonuses</dt>
				<dd>
					<ol>
						<li>5% bonus attack strength</li>
					</ol>
				</dd>

				<dt>Strategy</dt>
				<dd>
					The game involves Player-vs-Player and Player-vs-Computer combat. The attack bonus is only for offensive strikes. Players who plan on ammasing a large millitary and attacking planets often would be suited for this class
				</dd>

				<dt>Story</dt>
				<dd>
					[story about the general]
				</dd>

				<dt>Select</dt>
				<dd>
					You are about to select the General Class.<br><btn class='btn btn-primary'>Select <i class='icon-double-angle-right'></i></btn>
				</dd>
			</dl>
			
			</div>
	</div>
		<div class='page-wrap' id='col2'>
		
			<div class="info-col">
			
				<h2>Citizen</h2>
				
				<a class="image citizen">View Image</a>
				
				<dl>
				<dt>Bonuses</dt>
				<dd>
					This is where the bonuses HTML Goes
				</dd>

				<dt>Strategy</dt>
				<dd>
					Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
					aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
					commodo consequat.
				</dd>

				<dt>Story</dt>
				<dd>
					Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
					aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
					commodo consequat.
				</dd>

				<dt>Select</dt>
				<dd>
					You are about to select the Geologist Class.<br><btn class='btn btn-primary'>Select <i class='icon-double-angle-right'></i></btn>
				</dd>
			</dl>
			
			</div>
			
			<div class="info-col">
			
				<h2>Pilot</h2>
				
				<a class="image pilot">View Image</a>
				
				<dl>
				<dt>Bonuses</dt>
				<dd>
					This is where the bonuses HTML Goes
				</dd>

				<dt>Strategy</dt>
				<dd>
					Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
					aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
					commodo consequat.
				</dd>

				<dt>Story</dt>
				<dd>
					Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
					aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
					commodo consequat.
				</dd>

				<dt>Select</dt>
				<dd>
					You are about to select the Geologist Class.<br><btn class='btn btn-primary'>Select <i class='icon-double-angle-right'></i></btn>
				</dd>
			</dl>
			
			</div>

		<div class="info-col">
		
			<h2>Engineer</h2>
			
				<a class="image engineer">View Image</a>
			
			<dl>
				<dt>Bonuses</dt>
				<dd>
					This is where the bonuses HTML Goes
				</dd>

				<dt>Strategy</dt>
				<dd>
					Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
					aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
					commodo consequat.
				</dd>

				<dt>Story</dt>
				<dd>
					Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
					aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
					commodo consequat.
				</dd>

				<dt>Select</dt>
				<dd>
					You are about to select the Geologist Class.<br><btn class='btn btn-primary'>Select <i class='icon-double-angle-right'></i></btn>
				</dd>
			</dl>
		
		</div>
	  </div>
			
	</div>

</body>

</html>