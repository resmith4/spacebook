<?php session_start();
include 'cookout.php';
include_once 'functions.php';
include_once 'objects.php';

if($_SESSION['auth'] != 'yes'){
	include "spash_screen.php";die();
}

updatePlanetResources($_SESSION['activePlanet']);
updatePlanetShipConstruction($_SESSION['activePlanet']);

if($_POST['action'] == 'buildShips'){
	$message = addToShipBuildQueue($_SESSION['activePlanet'],$_POST['shipClassID'],$_POST['numberOfShips']);
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Spacebook</title>
	<?php 	include 'linksAndScripts.php';
			include 'templates/floatingStars_JS.php';
	?>
</head>
<body>
	<?php
	include 'templates/floatingStars_HTML.php';
	$resources = echoAvailableResources($_SESSION['activePlanet']);
	echoPlanetLocation($_SESSION['activePlanet']);
	?>

	<div id="page-wrap">
		<div class='container-fluid'>
			<div class='row-fluid'>
				<?php include "templates/spacebookHeader.php" ?>
			</div>
			<div style='margin-top:15px' class='row-fluid'>
				<div class='span3'>
					<?php include "templates/infoSidebar.php" ?>
				</div>
				<div class='span9'>
					<?php var_dump($_POST) ?>
					<?php echo $message ?>
					<?php include 'templates/shipyardInfo.php' ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
