<?php
include 'cookout.php';
$getFleets = $db->prepare("SELECT * FROM `fleets` WHERE `userID` = ?");
$getFleets->execute(array($_SESSION['UID']));
$userFleets = array();

while($row = $getFleets->fetch(PDO::FETCH_ASSOC)){
        array_push($userFleets,$row);
}

$now = time();
$tableData = array();
foreach($userFleets as $value){
     $arrival = strtotime($value['arrival']);
     if($now > $arrival){
     	//arrival time has passed
     	processFleetArrival($value['ID']);
     }else{
        $secondsLeft = $arrival - $now;
     	$timeLeft = secondsLeftIntoTime($secondsLeft);
        $from = getPlanetLocationString($value['originPlanetID']);
        $to = getPlanetLocationString($value['targetPlanetID']);
        $ships_activeFleets = generateShipsFromFleetRow($value);
        if(count($ships_activeFleets) > 1){
            $tooltipTitle = "";
            foreach($ships_activeFleets as $value2){
                $shipName = substr($value2[0],6);

                $tooltipTitle = $tooltipTitle."<br>$shipName - $value2[1]";
            }
            $shipList = "<a href='#' id='shipTooltip' data-toggle='tooltip' title='$tooltipTitle' data-html='true'>Info</a>";
        }else{
            $shipList = substr($ships_activeFleets[0][0],6)." - ".$ships_activeFleets[0][1];
        }
        if($value['type'] != "Return"){
	        $actionButton = "<a href='fleets.php?action=cancelFleet&fleetID=".$value['ID']."' class='btn btn-danger btn-mini'>Cancel</a>";
        }else{
        	$actionButton = "";
        }

        array_push($tableData,array($timeLeft,$value['type'],$from,$to,$shipList,$actionButton));
     }
}
?>

<table class='table fleet-table table-condensed'>
        <thead>
                <tr>
			<?php
				if (!isset($tabledata)) {
			?>
			<th colspan="6" style="font-size:14px; text-align:center;">There are no active fleets.</th>
			<?php
				} else {
			?>
                        <th>Time</th>
                        <th>Type</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Fleet</th>
			<th>Actions</th>
			<?php
				}
			?>
                </tr>
        </thead>
        <tbody>
            <?php foreach($tableData as $value){
                echo "<tr>";
                echo "<td>$value[0]</td>";
                echo "<td>$value[1]</td>";
                echo "<td>$value[2]</td>";
                echo "<td>$value[3]</td>";
                echo "<td>$value[4]</td>";
                echo "<td>$value[5]</td>";
                echo "<tr>";
            }
            ?>
        </tbody>
</table>

<script>
    $("#shipTooltip").tooltip({
        html:'true'
    })
</script>
