<?php
include 'cookout.php';
$getEmpireResearchProjects =  $db->prepare("SELECT * FROM `activeResearch` WHERE `userID` = ?");
$getEmpireResearchProjects->execute(array($_SESSION['UID'])) or die(var_dump($db->errorInfo()));

$num = $getEmpireResearchProjects->rowCount();

if($num != 0){
	?>
	<p class='planet-overview-header'>Active Research</p>
	<ul class='progress-display'>
	<?php
	while($row = $getEmpireResearchProjects->fetch(PDO::FETCH_ASSOC)){
		$techInfo = getTechInfoFromBaseTechAndLevel($row['baseTechID'],$row['toLevel']);
		$title = "Researching: ".$techInfo['name'];
		$cancelButton = array();
		$cancelButton['link'] = "research.php?action=cancelResearch&projectID=".$row['ID'];
		$cancelButton['classes'] = "btn-danger";
		$cancelButton['text'] = 'Cancel';
		generateProgressBar($row['startTime'],$row['endTime'],$title,$cancelButton);
	}
}

?>
	</ul>