<?php
$pages = array(
	'Home'=>'admin.php');
function generateNavLinks($pages){
	$pageName = basename($_SERVER['PHP_SELF']);
	foreach($pages as $key=>$value){
		if($value == $pageName){
			$active = "class='active'";
		}
		echo "<li $active ><a href='$value'>$key</a></li>\n";

	}
}
?>
<div class='navbar navbar-inverse'>
	<div class='navbar-inner'>
		<a class='brand' href='admin.php'>Spacebook:Admin</a>
		<ul class='nav'>
			<?php generateNavLinks($pages); ?>
		</ul>
		<a class='brand pull-right' href='index.php'>Back To Game <i class='icon-double-angle-right'></i></a>
	</div>
</div>