<?php 
	$buildings = getPlanetBuildingInfo($_SESSION['activePlanet']);
	$projects = getCurrentConstructionProjects($_SESSION['activePlanet']);
?>
<?php if(count($projects) > 0){ ?>

	<p class='planet-overview-header'>Active Construction Projects</p>
		<ul class='progress-display'>
			<?php 
			foreach($projects as $value){ 
				$title = "Constructing:".$value['name'];
				$actionButton = array(
					'classes'=>'btn-danger',
					'link'=>"buildings.php?action=cancelBuilding&ID=".$value['ID'],
					'text'=>'cancel');
				generateProgressBar($value['start'],$value['end'],$title,$actionButton); 
			}
		} //closing up if(count($projects) > 0) 
		?>
		</ul>


<?php foreach($buildings as $value){ ?>
<div class='row-fluid building-info-box'>
	<div class='span2 mission-img'>
		<img src='../img/missionIcons/default.png'>
	</div>
	<div class='span7 mission-body'>
		<div class='mission-info'>
			<p class='mission-name'><?php echo $value['name']; ?></p>
			<p class='mission-info'><?php echo $value['description'] ?></p>
		</div>
	</div>
	<div class='pull-right span3 building-info'>
		<ul>
			<?php
				foreach($value['production'] as $productionValue){
					echo "<li class='resource-pane ".$productionValue['type']."-background'>".$productionValue['amount']."</li>";
				}
				foreach($value['consumption'] as $consumptionValue){
					echo "<li class='resource-pane ".$consumptionValue['type']."-background'>".$consumptionValue['amount']."</li>";
				}
			?>
		</ul>
	</div>
	<div class='row-fluid building-extensions'>
		<?php foreach($value['modules'] as $moduleValue){ ?>
		<div class='span4 building-extension-info'>
			<p><?php echo $moduleValue['name']; ?></p>
			<ul class='resource-listing'>
				<?php foreach($moduleValue['buildCost'] as $buildCostValue){
					//engage value fix if the type is time
					if($buildCostValue['type'] == 'time'){
						$amount = secondsLeftIntoTime($buildCostValue['amount']);
					}else{
						$amount = "-".$buildCostValue['amount'];
					}
					echo "<li class='resource-pane resource-pane-small ".$buildCostValue['type']."-background'>".$amount."</li>";
				} ?>
			</ul>
			<ul class='resource-listing'>
				<?php
					foreach($moduleValue['productionModification'] as $productionValue){
						echo "<li class='resource-pane resource-pane-small ".$productionValue['type']."-background'>".$productionValue['amount']."</li>";
					}
					foreach($moduleValue['consumptionModification'] as $consumptionValue){
						echo "<li class='resource-pane resource-pane-small ".$consumptionValue['type']."-background'>".$consumptionValue['amount']."</li>";
					}
				?>
			</ul>
			<a href='<?php echo "buildings.php?action=constructBuilding&buildingID=".$moduleValue['ID']."&planetID=".$_SESSION['activePlanet'] ?>' class='btn btn-small btn-success'>Build</a>
		</div>
		<?php } //closing up the module foreach ?>
	</div>
</div>

<?php } //closing up the foreach ?>