<?php
$availableRobots = checkBuildableRobots($_SESSION['activePlanet']);
?>

<?php
$num = count($availableRobots);

if($num == 0){ ?>
	<div class='next-mission-box'>
		<p>There are no availble robots for construction. Go to the <a href='tech.php'>tech</a> page and see what technology and buildings that are needed to build robots
<?php } //closing up the if($num == 0) ?>