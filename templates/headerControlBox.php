<?php
	$pagesLeft = array(
		"Home" => "index.php",
		"Events" => "events.php",
		"Tasks" => "tasks.php",
		"Goals" => "goals.php",
		"Workgroups" => "workgroups.php"
		);
	$pagesRight = array(
		"Achivements" => "achivements.php",
		"Advisors" => "advisors.php",
		"Profile" =>"profile.php",
		"Log Out" => "logout.php"
		);

	$current = basename($_SERVER['PHP_SELF']);

?>
<div class='navbarDiv span4' id='logo'>
	<p class='logo'>Project Raptor</p>
	<div class='row-fluid'>
		<div class='span6'>
			<ul class="nav nav-pills nav-stacked">
				<?php foreach($pagesLeft as $key=>$value){
					if($value == $current){
						echo "<li class='active'><a href='$value'>$key</a></li>";
					}else{
						echo "<li><a href='$value'>$key</a></li>";
					}
				}
				?>
			</ul>
		</div>
		<div class='span6'>
			<ul class='nav nav-pills nav-stacked'>
				<?php foreach($pagesRight as $key=>$value){
					if($value == $current){
						echo "<li class='active'><a href='$value'>$key</a></li>";
					}else{
						echo "<li><a href='$value'>$key</a></li>";
					}
				}
				?>
			</ul>
	</div>
</div>
</div>