<?php
	$fields = getPlanetFields($_SESSION['activePlanet']);
	$fleetInfo = getFleetInfo($_SESSION['UID']);
	$productionInfo = getProductionInfo($_SESSION['activePlanet']);
?>
<div class='planet-info-pane'>
	<p class='planet-info-pane-header'>Info</p>
	<p>Fields</p>
	<p class='info-pane-output'><?php echo $fields ?></p>
	<p>Fleets Used</p>
	<p class='info-pane-output'><?php echo $fleetInfo ?></p>
	<p>Production</p>
	<p class='info-pane-output ore-background'><?php echo $productionInfo['ore']; ?></p>
	<p class='info-pane-output crystal-background'><?php echo $productionInfo['crystal']; ?></p>
	<p class='info-pane-output hydrogen-background'><?php echo $productionInfo['hydrogen']; ?></p>
	<p>Energy</p>
	<p class='info-pane-output energy-background'><?php echo $productionInfo['energyUse']; ?></p>
</div>