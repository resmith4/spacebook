<?php $inventory = getUserInventory($_SESSION['UID']); ?>
<h3 class='center'>Item Inventory</h3>
<table class='table'>
	<tr>
		<th>Info</th>
		<th>Amount</th>
	</tr>
	<?php foreach($inventory as $value){ ?>
	<tr>
		<td><?php echo $value['name'].": ".$value['description']; ?></td>
		<td><?php echo $value['amount'] ?></td>
	</tr>
	<?php { // closing up the $foreach($inventory as $value) ?>