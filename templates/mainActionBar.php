<?php
	$empireName = getEmpireName($_SESSION['UID']);
	$planetName = getPlanetName($_SESSION['activePlanet']);
	$civActions = array(
		"Home",
		"Fleets",
		"Scans",
		"Missions",
		"Tech",);
	$planetActions = array(
		"Buildings",
		"Shipyard",
		"Research",
		"Inventory");



?>

<span class='civilization-name'><?php echo $empireName ?></span>
<ul class='civ-action-row'>
	<div class='civ-action-row-wrapper'>
		<?php foreach($civActions as $value){
			$lower = strtolower($value);
			echo "<li><a href='$lower.php'>$value</a></li>";
		} ?>
	</div>
</ul>

<ul class='planet-action-row'>
	<?php foreach($planetActions as $value){
		$lower = strtolower($value);
		echo "<li><a href='$lower.php'>$value</a></li>";
	} ?>
</ul>
<span class='planet-name'><?php echo $planetName ?></span>