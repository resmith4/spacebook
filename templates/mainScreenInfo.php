<div class = "row-fluid">
	<div class='home-screen-planet-img span6'>

		<img class='center' src='/img/planets/planet18.png'/>
		<h1 class='center'>Planet Name</h1>
	</div>
	<div class='span6'>
		<!Future home of planet bio information, etc.>
	</div>
</div>
<br>
<br>
<div class='row-fluid'>
	<p class='planet-overview-header'>Current planet activities</p>
		<ul class='progress-display'>
	<?php
		updateUser($_SESSION['UID']);
		$activities = getPlanetActivities($_SESSION['activePlanet']);

		foreach($activities as $value){
//			if($value['startTime'] < time()){
				$timeLeft = $value['endTime'] - time();
				if($timeLeft < 0){
					$timeLeft = 0;
				}
				$timeLeft = secondsLeftintoTime($timeLeft);
				$percentage = (time() - $value['startTime']) /($value['endTime'] - $value['startTime']) * 100;
				$percentage = floor($percentage);
				if($percentage > 100){
					$percentage = 100;
				}
				echo "<li>";
					echo $value['title'];
					foreach($value['actionLinks'] as $actionLinkValue){
						echo "<a href='actionLinks.php?$actionLinkValue?".$actionLinkValue['link']."' class='progress-bar-button btn btn-mini btn-primary pull-right'>".$actionLinkValue['action']."</a>";
					}
					echo "<br>";
					echo "<div class='progress-bar-outer'>";
						echo "<span>".$percentage."% ($timeLeft)</span>";
						echo "<div style='width:".$percentage."%' class='progress-bar-inner'>";
						echo "<div class='progress-bar-background'>";
					echo "</div>";
				echo "</li>";
			}
//		}
	?>
</div>
</div>
