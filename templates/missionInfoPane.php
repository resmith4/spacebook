<?php 
	$missions = getAvailableMissions($_SESSION['UID'],$_SESSION['activePlanet']);
	$construction = getCurrentConstructionProjects($_SESSION['activePlanet']);
?>
	<?php foreach($missions as $value){?>
		<div class='row-fluid mission-select-box'>
			<div class='span2 mission-img'>
				<img src='../img/missionIcons/default.png'>
			</div>
			<div class='span7 mission-body'>
				<div class='mission-info'>
					<p class='mission-name'><?php echo $value['name']; ?></p>
					<p class='mission-info'><?php echo $value['description']; ?></p>
				</div>
				<div class='mission-details'>
					<div class='mission-resources mission-info-pane'>
						<p class='ore-background'>+<?php echo $value['resources']['ore']; ?></p>
						<p class='crystal-background'>+<?php echo $value['resources']['crystal']; ?></p>
						<p class='hydrogen-background'>+<?php echo $value['resources']['crystal']; ?></p>
					</div>
						<ul class='mission-requirements'>
							<?php 
								foreach($value['requirements'] as $requirementValue){
									echo "<li><span>".$requirementValue['number']."</span> ".$requirementValue['name']."</li>";
								}
						?>
					</ul>
				</div>
			</div>
			<div class='pull-right span3 mission-actions'>
				<a href="missions.php?action=startMission&missionID=<?php echo $value['ID']; ?>" class='btn btn-success'>Dispatch</a>
				<br><br>
				<div class='btn-group'>
					<button class='btn-mini btn btn-inverse'><<</button><button class='btn-mini btn btn-inverse disabled'>1</button><button class='btn-mini btn btn-inverse'>>></button><button class='btn btn-mini btn-inverse'>M</button>
				</div>
				<br><br>
				<p class=''>Time:<span class='info-pane-output'><?php echo $value['resources']['time']; ?></span></p>
			</div>
		</div>
	<?php } //closing up the mission creation box foreach ?>

					<div class='next-mission-box'>
						<p>You need 2 Vespa class ships, and Research Tech IV for the next mission</p>
					</div>