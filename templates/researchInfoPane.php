<?php
	$techs = getEmpireResearchList($_SESSION['UID']);
	foreach($techs as $value){
		$time = secondsLeftIntoTime($value['resources']['time']);
		##TEMPLATE START##
	?>
	<div class='row-fluid mission-select-box'>
		<div class='span2 mission-img'>
			<img src='../img/missionIcons/default.png'>
		</div>
		<div class='span7 mission-body'>
			<div class='mission-info'>
				<p class='mission-name'><?php echo $value['name'] ?></p>
				<p class='mission-info'><?php echo $value['description'] ?></p>
			</div>
		</div>
		<div class='pull-right span3 building-info center'>
			<a href="<?php echo 'research.php?action=startResearch&techID='.$value['ID'] ?>" class='btn btn-success action-button pull-right'>Start</a>
			<ul class='resource-listing'>
				<li class='resource-pane ore-background'><?php echo $value['resources']['ore'] ?></li>
				<li class='resource-pane crystal-background'><?php echo $value['resources']['crystal'] ?></li>
				<li class='resource-pane hydrogen-background'><?php echo $value['resources']['hydrogen'] ?></li>
				<li class='resource-pane'><?php echo $time ?></li>
			</ul>
		</div>
		<div class="ship-stats">
			<ul class='research-unlocks'>
				<?php
					foreach($value['prereqs'] as $value2){
						echo "<li>$value2</li>";
					}
				?>
			</ul>
		</div>
			
	</div>



	<?php

		##TEMPLATE END##

	} //closes up the foreach loop
?>