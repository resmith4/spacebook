<?php
	$resources = getPlanetResourceArray($_SESSION['activePlanet']);
	extract($resources);

?>
<ul class='resource-display'>
	<span class='resource-display-wrapper'>
		<li>Resources</li>
		<li class='ore-color'><?php echo $ore ?> <img src='/img/resources/ore.png'></li>
		<li class='crystal-color'><?php echo $crystal ?> <img src='/img/resources/crystal.png'></li>
		<li class='hydrogen-color'><?php echo $hydrogen ?> <img src='/img/resources/hydrogen.png'></li>
	</span>
</ul>