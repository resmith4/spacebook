<?php
###checking to see if a planet was selected to view###

/*

tarGal - Target Galaxy
tarSys - Target System
tarPla - Target Planet

*/
if(((isset($_POST['tarGal']) && $_POST['tarGal'] != null) && (isset($_POST['tarSys']) && $_POST['tarSys'] != null)) || (isset($_GET['tarGal']) && isset($_GET['tarSys']))){
	if($_POST['tarGal'] != null){
		$tarGal = $_POST['tarGal'];
		$tarSys = $_POST['tarSys'];
		$tarPla = $_POST['tarPla'];
	}else{
		$tarGal = $_GET['tarGal'];
		$tarSys = $_GET['tarSys'];
		$tarPla = $_GET['tarPla'];
	}
}else{
	$planetArray = getPlanetLocationArray($_SESSION['activePlanet']);
	$tarGal = $planetArray['galaxy'];
	$tarSys = $planetArray['system'];
	$tarPla = $planetArray['planet'];
}

?>

<div class='row-fluid'>
	<div class='span6'>
		<div class='scan-locator input-append'>
			<input name='tarGal' id='tarGal' value='<?php echo $tarGal; ?>' type='text'><input name='tarSys' id='tarSys' value='<?php echo $tarSys; ?>' type='text'><input name='tarPla' id='tarSys' value='<?php echo $tarPla; ?>' type='text'><button class='btn btn-inverse'>Scan</button>
		</div>
	</div>
	<div class='span6'>
		<div class='btn-group'>
			<?php
			//print the back scan button if not at the front of the system
				if(($tarSys - 1) > 0){
					$newSys = $tarSys - 1;
					echo "<a href='scans.php?tarGal=$tarGal&tarSys=$newSys' class='btn btn-inverse'><<</a>";
				}
			?>
			<button class='btn btn-inverse disabled'>System <?php echo "$tarGal:$tarSys" ?></button>
			<?php
			//print the front scan button if not at the back of the system
				if(($tarSys + 1) <501){
					$newSys = $tarSys + 1;
					echo "<a href='scans.php?tarGal=$tarGal&tarSys=$newSys' class='btn btn-inverse'>>></a>";
				}
			?>
		</div>
	</div>
</div>
<?php
	$systemArray = getSystemScanArray($tarGal,$tarSys);
?>
 <table class='table planet-table table-consensed'>
	<thead>
		<tr><th>#</th><th>Name</th><th>Owned By</th><th>Status</th><th>Alliance</th><th>Actions</th></tr>
	</thead>
	<tbody>
		<?php generateScanTableRows($systemArray) ?>
	</tbody>
</table>		
