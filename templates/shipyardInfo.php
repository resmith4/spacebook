<?php $buildableShips = getBuildableShips($_SESSION['activePlanet']); ?>
<?php foreach($buildableShips as $TypeValue){ ?>
<?php foreach($TypeValue['ships'] as $value){ ?>
<div class='row-fluid building-info-box'>
	<div class='span2 mission-img'>
		<img src='../img/missionIcons/default.png'>
	</div>
	<div class='span7 mission-body'>
		<div class='mission-info'>
			<p class='mission-name'><?php echo $value['name'] ?></p>
			<p class='mission-info'><?php echo $value['description'] ?></p>
		</div>
	</div>
	<div class='pull-right span3 building-info'>
		<div class='input-append'><input class='ship-number' value='1' max='<?php echo $value['maxBuildNumber'] ?>' type='number'>
			<button class='btn btn-primary ship-build-button' ID='<?php echo $value['ID'] ?>'><i class='icon-upload-alt'></i></button>
		</div>
	</div>
	<div class="ship-stats">
		<table class='table-ship-stats'>
			<thead><th></th><th>Base</th><th>Current</th></thead>
			<tbody>
				<?php foreach($value['stats'] as $statValue){ ?>

				<tr><td><?php echo $statValue['name'] ?></td><td><?php echo $statValue['value'] ?></td><td class='ship-stat-current'><?php echo $statValue['value'] ?></td></tr>
				<?php } //closing the foreach ($value['stats']) ?>
			</tbody>
		</table>
	</div>
		
</div>
<?} //closing the foreach($TypeValue['ships'] as $value) ?>
<?} //closing the foreach($buildableShips as $value) ?>

<script>
	$(".ship-build-button").click(function(){
		shipClass = $(this).attr("ID"); //get the ship class ID
		//get the num of ships that the user wants to dispatch
		IDString = "#"+shipClass;
		numberOfShips = $(IDString).parent().children('input').val();
		form = new JSForm();
		form.addItem("shipClassID",shipClass);
		form.addItem("numberOfShips",numberOfShips);
		form.submitForm("shipyard.php","buildShips");
	});
</script>
