<?php $items = getShopItems($userID); ?>
<h2 class='center'>Shop</h2>
<ul class="nav nav-pills">
	<?php foreach($items as $category){ ?>
		<li><a href='#tab<?php echo $category['categoryName'] ?>' data-toggle='tab'><?php echo $category['categoryName'] ?></a></li>
	<?php } //closing the foreach($items as $category) ?>
</ul>
<div class="tab-content">
	<div class='tab-pane active' id='tabIntro'>
		<p>Select a category of item to purchase</p>
	</div>
	<?php foreach($items as $category){ ?>
		<div class='tab-pane' id='tab<?php echo $category['categoryName'] ?>'>
			<table class='table'>
				<tr>
					<th>Item</th>
					<th>Amount</th>
					<th>Cost</th>
					<th>Buy</th>
				</tr>
				<?php foreach ($category['items'] as $item){ ?>
					<tr>
						<td><?php echo $item['name'].": ".$item['description'] ?></td>
						<td><?php echo $item['itemAmount'] ?></td>
						<td><?php echo $item['purchaseCost'] ?></td>
						<td><a href='shop.php?action=buyItem&itemID=<?php echo $item['ID'] ?>' class='btn btn-success'>Buy</a></td>
					</tr>
				<?php } //closing up the ($category['items'] as $item)?>
			</table>
		</div>
	<?php } //closing the foreach($items as $category) ?>
</div>
