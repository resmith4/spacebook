<?php
	$techInfo = getEmpireTechInfo($_SESSION['UID']);
	var_dump($categories);
?>


<ul class='nav nav-pills'>
	<?php foreach($techInfo as $value){
		echo "<li><a href='#tab".$value['name']."' data-toggle='tab'>".$value['name']."</a></li>";
	}?>
</ul>

<div class='tab-content'>
	<div class='tab-pane active' id='tabDefault'>
		<div class='next-mission-box'>
			<p>Select a category to see the results</p>
		</div>
	</div>
	<?php foreach($techInfo as $value){ ?>
		<div class='tab-pane' id='tab<?php echo $value['name']; ?>'>
			<ul>
			<?php foreach($value['techs'] as $techValue){ ?>
				<li><?php echo $techValue['name'] ?>
					<ul>
					<?php foreach($techValue['preReqs'] as $preReqValue){ ?>
						<li><?php echo $preReqValue['name']?></li>
					<?php } //closing up the forach($techValue['preReqs']) ?>
					</ul>
				</li>
			</ul>

			<?php } //closing up the foreach($value['techs']) ?>
		

		</div>
	<?php } //closing up foreach($categories as $value ?>
	
</div>