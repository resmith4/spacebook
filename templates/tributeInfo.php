<?php
$prizes = getTributePrizes();
?>
<h3 class='center'>Tribute Info</h3>
<a href='tribute.php?action=spin' class='center btn btn-large btn-success'>SPIN</a>
<h4>Available Prizes</h4>
<table class='table'>
	<tr>
		<th>Prize</th>
		<th>Prize Amount</th>
		<th>Chance of Winning</th>
	</tr>
	<?php foreach($prizes as $value){ ?>
	<tr>
		
		<td><?php echo $value['name'].": ".$value['description']?></td>
		<td><?php echo $value['amount'] ?></td>
		<td><?php echo $value['rollChance'] ?></td>
		
	</tr>
	<?php } //closing the foreach($prizes as $value) ?>
</table>